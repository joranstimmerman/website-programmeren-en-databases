<!doctype html>
<html lang="en">

<head>
    <title>JEM Records | Accounts beheren</title>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/standard_page.css">
    <link rel="stylesheet" type="text/css" href="css/navbar.css">
    <link rel="stylesheet" type="text/css" href="css/footer.css">
    <link rel="stylesheet" type="text/css" href="css/admin_accounts.css">
    <link rel="icon" href="images/ui_icons/tabicoon.png" type="image/png">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/myScripts.js"></script>
</head>

<body>
    <?php include 'phpinclude/header.php'; ?>

    <?php
        include "php/opendb.php";
        include "php/jem_queries.php";
        include "php/admin_accounts_form.php";

        /* Verify authenticaton */
        if ($_SESSION['authentication'] != 2) {
            header("Location: 401.php");
        }
    ?>

    <div id="main_content">
        <h2>Accounts beheren</h2>
        <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" method="POST">
            <div class="search_users">
                <div class="form_names"><p>Klantnummer:</p></div>
                <div class="search_user_input">
                    <input type="number" name="customer_id" min ="1" step ="1"><br>
                </div>
            </div>
            <div class="search_users">
                <div class="form_names"><p>Achternaam:</p></div>
                <div class="search_user_input">
                    <input type="text" name="last_name"><br>
                </div>
            </div>
            <div class="search_user_btn">
                <input type="submit" value="Zoek accounts"><br>
            </div>
        </form>

        <div class="list_accounts">
        <!-- Get users and their information from database and check if there are results -->
        <?php if ($admin_accounts->rowCount() < 1) { ?>
        <p><b>Geen accounts met de opgegeven gegevens gevonden.</b></p>
        <?php } ?>
            <?php while($admin_accounts_row = $admin_accounts->fetch(PDO::FETCH_ASSOC)) { ?>
            <div class="account">
                <div class="account_id">
                    <h3><?php echo $admin_accounts_row['gebruiker_id']?></h3>
                </div>

                <div class="account_info">
                    <h3>Gegevens</h3>
                    <p> Klantnummer: <?php echo $admin_accounts_row['gebruiker_id']?> </p>
                    <p>
                        Naam: <?php echo $admin_accounts_row['voornaam'] ?>
                              <?php echo $admin_accounts_row['tussenvoegsel'] ?>
                              <?php echo $admin_accounts_row['achternaam'] ?></p>
                    <p>
                        Adres: <?php echo $admin_accounts_row['straatnaam'] ?> <?php echo $admin_accounts_row['huisnummer'] ?>
                               <?php echo $admin_accounts_row['huisnummer_toevoeging'] ?>
                               <?php echo $admin_accounts_row['postcode'] ?> <?php echo $admin_accounts_row['plaats'] ?>
                    </p>
                    <p> E-mail: <div class="mail"><?php echo $admin_accounts_row['email']?></div></p>
                </div>

                <div class="account_type">
                    <h3>Accounttype</h3>
                    <form action ="<?php echo htmlspecialchars("php/change_account_type.php");?>" method ="post">
                    <input type="hidden" name="customer_id" value=<?php echo $admin_accounts_row['gebruiker_id']?>/>
                    <!-- Make accoountype change possible -->
                    <select name="account_type">
                        <?php if ($admin_accounts_row['account_type'] == 0) { ?>
                                <option value = "0">Inactief</option>
                                <option value = "1">Klant</option>
                                <option value = "2">Beheerder</option>
                        <?php }
                            else if ($admin_accounts_row['account_type'] == 1) { ?>
                                <option value = "1">Klant</option>
                                <option value = "0">Inactief</option>
                                <option value = "2">Beheerder</option>
                        <?php }
                            else if ($admin_accounts_row['account_type'] ==  2) { ?>
                                <option value="2">Beheerder</option>
                                <option value = "0">Inactief</option>
                                <option value="1">Klant</option>
                        <?php } ?>
                    </select>
                    <br>
                    <input type ="submit" value="OK">
                    </form>
                </div>
            </div>
            <?php  } ?>
        </div>
    </div>

    <?php include 'phpinclude/footer.php'; ?>
</body>

</html>