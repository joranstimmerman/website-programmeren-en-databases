<!doctype html>
<html lang="en">

<head>
    <title>JEM Records | Betaalmethoden</title>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/standard_page.css">
    <link rel="stylesheet" type="text/css" href="css/navbar.css">
    <link rel="stylesheet" type="text/css" href="css/footer.css">
    <link rel="stylesheet" type="text/css" href="css/payment_methods.css">
    <link rel="icon" href="images/ui_icons/tabicoon.png" type="image/png">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/myScripts.js"></script>
</head>

<body>
    <?php include 'phpinclude/header.php'; ?>

    <div id="main_content">
        <h2>Betaalmethoden</h2>
        <div id="title_payment">
            <p>Wij accepteren de volgende betaalmethoden:</p>
        </div>
        <div id="ideal" class="paymethod">
            <h3><img class="payment_logo" src="images/ui_icons/ideal.png" alt="iDeal icoon"> iDeal</h3>
            <p>
                Bij het gebruik van iDEAL wordt u tijdens het betalingsproces omgeleid naar de betaalomgeving van uw
                bank.
                U kan alleen via iDeal betalen als u gebruik maakt van internetbankieren bij de volgende banken: ABN
                AMRO,
                ASN Bank, Friesland Bank, ING, Rabobank, SNS Bank, SNS Regio Bank, Van Lanschot Bankiers en Triodos
                Bank.
                Bij deze betaalwijze wordt uw betaling direct afgeschreven van jouw rekening.
            </p>
        </div>
        <div id="paypal" class="paymethod">
            <h3><img class="payment_logo" src="images/ui_icons/paypal.png" alt="PayPal icoon"> PayPal</h3>
            <p>
                Als u kiest om met PayPal te betalen, wordt u tijdens het betalen doorgestuurd naar de PayPal website.
                Hier kan u inloggen op uw PayPal-account en de betaling bevestigen. Als de betalling is bevestigd
                zal er een autorisatie worden uitgevoerd van uw PayPal account. Vervolgens wordt uw bestelling verwerkt.
                Zodra u uw bestelling ontvangen hebt, zal het openstaande bedrag gecrediteerd worden van uw PayPal account.
            </p>
        </div>
        <div id="creditcard" class="paymethod">
            <h3>
                <img class="payment_logo" src="images/ui_icons/mastercard.png" alt="MasterCard icoon">
                <img class="payment_logo" src="images/ui_icons/visa.png" alt="Visa icoon"> Mastercard en Visa
            </h3>
            <p>Wij accepteren MasterCard en Visa. Het bedrag wordt gereserveerd en pas afgeschreven nadat uw artikel is geleverd.</p>
        </div>
    </div>

    <?php include 'phpinclude/footer.php'; ?>
</body>

</html>
