<!doctype html>
<html lang="en">

<head>
    <title>JEM Records | Mijn bestellingen</title>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/standard_page.css">
    <link rel="stylesheet" type="text/css" href="css/navbar.css">
    <link rel="stylesheet" type="text/css" href="css/footer.css">
    <link rel="stylesheet" type="text/css" href="css/orders.css">
    <link rel="icon" href="images/ui_icons/tabicoon.png" type="image/png">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/myScripts.js"></script>
</head>

<body>
    <?php include 'phpinclude/header.php'; ?>

    <?php
        include "php/opendb.php";
        include "php/jem_queries.php";

        /* Verify authentication and show content based on it */
        if (isset($_SESSION['authentication']) && $_SESSION['authentication'] > 0) {
            $cur_user = intval($_SESSION['user_id']);
            $order_query = $db->prepare(get_user_orders());

            $order_query->bindValue(1, $cur_user, PDO::PARAM_INT);
            $order_query->execute();
        }
        else if (!isset($_SESSION['authentication']))
        {
            header("Location: 401.php");
        }
        else if ($_SESSION['authentication'] < 1)
        {
            header("Location: register_status.php");
        }
    ?>

    <div id="main_content">
        <div class="order_list">
            <h2>Mijn bestellingen</h2>
            <?php if ($order_query->rowCount() < 1) { ?>
                <p>U heeft nog geen bestellingen</p>
            <?php } else {
                    while ($order_row = $order_query->fetch(PDO::FETCH_ASSOC)) { ?>
                    <div class="order">
                        <div class="order_img">
                            <h3><?php echo $order_row['bestel_datum'] ?></h3>
                            <img src=" <?php
                            /* Get the first album cover from an order */
                            $first_order_albums_query = $db->prepare(get_first_user_order_albums_query());
                            $first_order_albums_query->bindValue(1, intval($order_row['bestelling_id']), PDO::PARAM_INT);
                            $first_order_albums_query->execute();
                            $first_order_albums_row = $first_order_albums_query->fetch(PDO::FETCH_ASSOC);

                            echo $first_order_albums_row['album_cover'];
                            ?> " alt="<?php echo $first_order_albums_row['titel'] ?>">
                        </div>
                        <div class="order_info">
                            <h3>Bestelling informatie</h3>
                            <p>Status: <?php echo $order_row['status_tekst'] ?><br></p>
                            <p>Ordernummer: <?php echo $order_row['bestelling_id'] ?><br></p>
                            <p>Totaal: &euro; <?php
                                /* Calculate total price of the order */
                                $order_price_query = $db->prepare(get_order_total_query());
                                $order_price_query->bindValue(1, intval($order_row['bestelling_id']), PDO::PARAM_INT);
                                $order_price_query->execute();
                                $order_price_row = $order_price_query->fetch(PDO::FETCH_ASSOC);

                                echo $order_price_row['order_total'] ?></p>
                            <div class="order_btns">
                                <?php
                                    /* Make it possible to return albums of a specific order */
                                    $number_retour = $db->prepare("SELECT * FROM retour_verzending WHERE bestelling_bestelling_id = ?");
                                    $number_retour->bindValue(1, intval($order_row['bestelling_id']), PDO::PARAM_INT);
                                    $number_retour->execute();

                                    if ($number_retour->rowCount() < 1 && $order_row['bestelling_status_status_id'] == 3) {;
                                ?>
                                    <form action="return_product.php?order=<?php echo $order_row['bestelling_id'] ?>" method="post">
                                        <input type="submit" value="Retour sturen" />
                                    </form>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="order_info">
                            <h3>Producten</h3>
                            <div class="order_albums">
                                <table>
                                    <?php
                                        /* Get every album that corresponds with this order */
                                        $order_albums_query = $db->prepare(get_user_order_albums_query());
                                        $order_albums_query->bindValue(1, intval($order_row['bestelling_id']), PDO::PARAM_INT);
                                        $order_albums_query->execute();

                                        while ($order_albums_row = $order_albums_query->fetch(PDO::FETCH_ASSOC)) {
                                    ?>
                                        <tr>
                                            <td>
                                                <a href="product.php?album_id=<?php echo $order_albums_row['album_id'] ?>">
                                                    <?php echo $order_albums_row['artiest'] ?> -
                                                    <?php echo $order_albums_row['titel'] ?>
                                                </a>
                                                <span style="float:right">: <?php echo $order_albums_row['aantal'] ?></span>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                <?php }
                    } ?>
        </div>
    </div>

    <?php include 'phpinclude/footer.php'; ?>
</body>

</html>
