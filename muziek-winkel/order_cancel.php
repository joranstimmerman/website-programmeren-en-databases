<!doctype html>
<html lang="en">

<head>
    <title>JEM Records | Bestelling geannuleerd</title>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/standard_page.css">
    <link rel="stylesheet" type="text/css" href="css/navbar.css">
    <link rel="stylesheet" type="text/css" href="css/footer.css">
    <link rel="stylesheet" type="text/css" href="css/order_final.css">
    <link rel="icon" href="images/ui_icons/tabicoon.png" type="image/png">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/myScripts.js"></script>
</head>

<body>
    <?php include 'phpinclude/header.php'; ?>

    <?php
        /* Verify authenticaton */
        if (!isset($_SESSION['user_id'])) {
            header("Location: 401.php");
        }
    ?>

    <div id="main_content">
        <div id="title_final">
            <h2>Bestelling geannuleerd</h2>
            <p>
                U hebt niet betaald, dus uw bestelling is geannuleerd.
                <br>
                Binnen enkele seconden wordt u doorverwezen naar uw bestellingenoverzicht.
            </p>
        </div>
    </div>

    <!-- Redirect to user orders after 3 seconds -->
    <?php header("refresh: 3; url=user_orders.php"); ?>

    <?php include 'phpinclude/footer.php'; ?>
</body>

</html>