<!doctype html>
<html lang="en">

<head>
    <title>JEM Records | Retourneren</title>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/standard_page.css">
    <link rel="stylesheet" type="text/css" href="css/navbar.css">
    <link rel="stylesheet" type="text/css" href="css/footer.css">
    <link rel="stylesheet" type="text/css" href="css/return_product.css">
    <link rel="icon" href="images/ui_icons/tabicoon.png" type="image/png">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/myScripts.js"></script>
</head>

<body>
    <?php include 'phpinclude/header.php'; ?>

    <?php
        include 'php/opendb.php';
        include 'php/jem_queries.php';

        /* Verify authentication */
        if (!isset($_SESSION['user_id'])) {
            header("Location: 401.php");
        }

        /* Verify ordernumber */
        if (!isset($_GET['order'])) {
            header("Location: user_orders.php");
        }

        /* Get the to be returned order and its information from database */
        $bestelling_nummer = intval($_GET['order']);
        $error_msg = "";

        if (isset($_GET['error'])) {
            $error_msg = $_GET['error'];
        }

        $bestelling_albums = $db->prepare(get_user_order_albums_query());
        $bestelling_albums->bindValue(1, intval($bestelling_nummer), PDO::PARAM_INT);
        $bestelling_albums->execute();

        $number_ordered_album = $db->prepare(get_number_ordered_albums());
        $number_ordered_album->bindValue(1, intval($bestelling_nummer), PDO::PARAM_INT);
        $number_ordered_album->execute();

        /* Get return reason from database */
        $retour_reason = $db->query(get_retour_reden());
    ?>

    <div id="main_content">
        <div class="retour">
            <form action="<?php echo htmlspecialchars("php/retour_form.php?order=$bestelling_nummer"); ?>" method="POST">
                <div>
                    <h2>Retourneren</h2>
                    <p>
                        <br>
                        Wij vragen u om de volgende vragenlijst in te vullen.
                        U ontvangt van ons een e-mail met een retourlabel. Dit kunt u meezenden met het product dat u wilt
                        retourneren.
                    </p>

                    <br>

                    <h3>Betreft de volgende producten*</h3>
                    <div class="retour_table">
                        <table>
                            <!-- Make possibe to select an album to be returned with the amount -->
                            <?php while ($order_albums_row = $bestelling_albums->fetch(PDO::FETCH_ASSOC)) { ?>
                                <?php $aantal_row = $number_ordered_album->fetch(); ?>
                                <tr>
                                    <td>
                                        <a href="product.php?album_id=<?php echo $order_albums_row['album_id'] ?>">
                                            <?php echo $order_albums_row['artiest'] ?> -
                                            <?php echo $order_albums_row['titel'] ?>
                                        </a>
                                        <input class="check_return" type="number" min="0" max="<?php echo $aantal_row['aantal'] ?>" value="0" name="<?php echo $order_albums_row['album_id'] ?>">
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>

                <div class="retour_radio">
                    <br>
                    <h3>Reden voor retour*</h3>
                        <?php while ($retour_reason_row = $retour_reason->fetch(PDO::FETCH_ASSOC)) { ?>
                            <label class="radio_lbl">
                                <input type="radio" name="reden" value="<?php echo $retour_reason_row['reden_id'] ?>" <?php if (isset($_POST['reden'])) { echo "checked='checked'"; } ?>><?php echo $retour_reason_row['reden_text'] ?>
                            </label>
                        <?php } ?>
                    <br>
                </div>

                <div class="retour_form">
                    <h3>Opmerkingen</h3>
                    <div class="retour_field">
                        <textarea name="opmerking" rows="4" cols="60"><?php if(isset($_POST['textarea1'])) { echo htmlentities ($_POST['textarea1']); }?></textarea>
                    </div>
                </div>

                <div class="retour_form">
                    <p>*verplicht</p>
                </div>

                <br>

                <div class="submit_form">
                    <input type="submit" value="Verzend retourlabel">
                </div>

                <?php echo $error_msg; ?>
            </form>
        </div>
    </div>

    <?php include 'phpinclude/footer.php'; ?>
</body>

</html>