<!doctype html>
<html lang="en">

<head>
    <title>JEM Records | Retourverzendingen beheren</title>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/standard_page.css">
    <link rel="stylesheet" type="text/css" href="css/navbar.css">
    <link rel="stylesheet" type="text/css" href="css/footer.css">
    <link rel="stylesheet" type="text/css" href="css/admin_orders.css">
    <link rel="icon" href="images/ui_icons/tabicoon.png" type="image/png">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/myScripts.js"></script>
</head>

<body>
    <?php include 'phpinclude/header.php'; ?>

    <?php
        include "php/jem_queries.php";
        include "php/opendb.php";
        include "php/find_retours_form.php";

        /* Verify authenticaton */
        if ($_SESSION['authentication'] != 2) {
            header("Location: 401.php");
        }

        /* Get retour status from database */
        $retour_status_query = $db->query(get_retour_status());
        $retour_status_data = $retour_status_query->fetchAll();
    ?>

    <div id="main_content">
        <h2>Retourverzendingen beheren</h2>
        <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" method="post">
            <div class="search_order">
                <div class="input_names"><p>Retournummer: </p></div>
                <div class="input_field">
                    <input type="number" name="retour_id" min ="1" step ="1"><br>
                </div>
            </div>
            <div class="search_order">
                <div class="input_names"><p>Ordernummer: </p></div>
                <div class="input_field">
                    <input type="number" name="order_id" min ="1" step ="1"><br>
                </div>
            </div>
            <div class="search_order">
                <div class="input_names"><p>Retour datum: </p></div>
                <div class="input_field">
                    <input type="date" name="retour_date"><br>
                </div>
            </div>
            <div class="search_order">
                <div class="input_names"><p>Retour status: </p></div>
                <div class="input_field">
                    <select name="find_status">
                        <option value=""></option>
                        <?php foreach ($retour_status_data as $retour_status_row) { ?>
                        <option value="<?php echo $retour_status_row["retour_status_id"]?>">
                        <?php echo $retour_status_row["retour_status_tekst"];?></option>
                        <?php } ?>
                    </select><br>
                </div>
            </div>
            <div class="search_order_btn">
                <input type="submit" value="Vind retourverzendingen"><br>
            </div>
        </form>

        <div class="list_orders">
            <!-- Get retours and their information from database and check if there are results -->
            <?php if ($admin_retours->rowCount() < 1) { ?>
            <p><b>Geen retourverzendingen met de opgegeven gegevens gevonden.</b></p>
            <?php } ?>
            <?php while($admin_retour_row = $admin_retours->fetch(PDO::FETCH_ASSOC)) { ?>
            <div class="one_order">
                <div class="img_order">
                    <h3><?php echo $admin_retour_row['datum']?></h3>
                    <img src=" <?php
                    $first_retour_albums_query = $db->prepare(get_first_retour_order());
                    $first_retour_albums_query->bindValue(1, intval($admin_retour_row['retour_id']), PDO::PARAM_INT);
                    $first_retour_albums_query->execute();
                    $first_retour_albums_row = $first_retour_albums_query->fetch(PDO::FETCH_ASSOC);

                    echo $first_retour_albums_row['album_cover'];
                    ?>" alt="<?php $first_retour_albums_row['titel'] ?>">
                </div>

                <div class="info_order">
                <h3>Retour informatie</h3>
                    <p>Retournummer: <?php echo $admin_retour_row['retour_id'] ?><br></p>
                    <p>Bestellingnummer: <?php echo $admin_retour_row['bestelling_bestelling_id'] ?><br></p>
                    <p>Reden:
                        <?php
                            $retour_redenen = $db->prepare(get_retour_reden_text());
                            $retour_redenen->bindValue(1, intval($admin_retour_row['retour_reden_reden_id']), PDO::PARAM_INT);
                            $retour_redenen->execute();
                            $retour_reden = $retour_redenen->fetch(PDO::FETCH_ASSOC);
                            echo $retour_reden['reden_text'];
                            ?><br>
                    </p>
                    <p>Opmerking: <?php echo $admin_retour_row['opmerking'] ?><br></p>
                </div>

                <div class="info_order">
                    <h3>Producten</h3>
                    <div class="album_orders">
                        <table>
                            <?php
                                /* Get the albums corresponding with a specific retour */
                                $retour_albums_query = $db->prepare(get_retour_albums());
                                $retour_albums_query->bindValue(1, intval($admin_retour_row['retour_id']), PDO::PARAM_INT);
                                $retour_albums_query->execute();

                                while ($retour_albums_row = $retour_albums_query->fetch(PDO::FETCH_ASSOC)) {
                            ?>
                            <tr>
                                <td>
                                    <a href="product.php?album_id=<?php echo $retour_albums_row['album_id'] ?>">
                                        <?php echo $retour_albums_row['artiest'] ?> -
                                        <?php echo $retour_albums_row['titel'] ?>
                                    </a>
                                    <span style="float:right">: <?php echo $retour_albums_row['aantal_retour'] ?></span>
                                </td>
                            </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>

                <div class="info_order">
                    <h3>Status</h3>
                    <form action="<?php echo htmlspecialchars("php/retour_status.php"); ?>" method="post">
                        <input type='hidden' name='retour_id_status' value=<?php echo $admin_retour_row['retour_id']?>/>
                        <!-- Make it possible to change retour status -->
                        <select name="status" class="status">
                            <?php
                                $retour_status_text = $db->prepare(get_retour_status_text());
                                $retour_status_text->bindValue(1, intval($admin_retour_row['retour_status_status_id']), PDO::PARAM_INT);
                                $retour_status_text->execute();
                                $retour_status_text_row = $retour_status_text->fetch(PDO::FETCH_ASSOC);
                            ?>
                            <option value="<?php $admin_retour_row['retour_status_status_id']?>"><?php echo $retour_status_text_row['retour_status_tekst']; ?></option>
                            <?php foreach ($retour_status_data as $retour_status_row) {
                                if ($admin_retour_row['retour_status_status_id'] != $retour_status_row['retour_status_id']) { ?>
                                    <option value="<?php echo $retour_status_row["retour_status_id"]?>"><?php echo $retour_status_row["retour_status_tekst"];?></option>
                            <?php } } ?>
                        </select><br>
                        <input id="status_btn" type = "submit" value="OK" />
                    </form>
                </div>
            </div>
            <?php  } ?>
        </div>
    </div>

    <?php include 'phpinclude/footer.php'; ?>
</body>

</html>