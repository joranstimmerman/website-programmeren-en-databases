<!doctype html>
<html lang="en">

<head>
    <title>JEM Records | Wachtwoord</title>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/standard_page.css">
    <link rel="stylesheet" type="text/css" href="css/navbar.css">
    <link rel="stylesheet" type="text/css" href="css/footer.css">
    <link rel="icon" href="images/ui_icons/tabicoon.png" type="image/png">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/myScripts.js"></script>
</head>

<body>
    <?php include 'phpinclude/header.php'; ?>

    <div id="main_content">
        <h2>Wachtwoord reset link verzonden</h2>
        <p>Klik de link in uw in de verzonden mail om uw wachtwoord te resetten</p>
        <br>
    </div>

    <?php include 'phpinclude/footer.php'; ?>
</body>

</html>