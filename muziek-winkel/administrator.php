<!doctype html>
<html lang="en">

<head>
    <title>JEM Records | Producten toevoegen</title>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/standard_page.css">
    <link rel="stylesheet" type="text/css" href="css/navbar.css">
    <link rel="stylesheet" type="text/css" href="css/footer.css">
    <link rel="stylesheet" type="text/css" href="css/administrator.css">
    <link rel="icon" href="images/ui_icons/tabicoon.png" type="image/png">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/myScripts.js"></script>
</head>

<body>
    <?php include 'phpinclude/header.php'; ?>

    <?php
        include "php/jem_queries.php";
        include "php/opendb.php";
        include "php/admin_form.php";

        /* Verify authenticaton */
        if ($_SESSION['authentication'] != 2) {
            header("Location: 401.php");
        }

        /* Get genres from database */
        $genre_query = $db->query(get_genre());
        $genre_data = $genre_query->fetchAll();
    ?>

    <div id="main_content">
        <h2> PRODUCT TOEVOEGEN</h2>
        <div id="success">
            <p><?php echo $success_message; ?></p>
        </div>
        <form enctype="multipart/form-data" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" method="post">
            <div class="admin_products">
                <div class="form_names"><p>* Titel:</p></div>
                <div class="input_field">
                    <input type="text" name="title" value="<?php echo $title?>">
                    <div class="error">
                        <p><?php echo $title_error;?></p>
                    </div>
                </div>
            </div>
            <div class="admin_products">
                <div class="form_names"><p>* Artiest:</p></div>
                <div class="input_field">
                    <input type="text" name="artist"  value="<?php echo $artist?>">
                    <div class="error">
                        <p><?php echo $artist_error;?></p>
                    </div>
                </div>
            </div>
            <div class="admin_products">
                <div class="form_names"><p>* Genre:</p></div>
                <div class="input_field">
                    <select name="genre">
                        <option value="<?php echo $genre?>"><?php echo $genre?></option>
                        <?php foreach ($genre_data as $genre_row) { ?>
                        <option value="<?php echo $genre_row["genre_id"]?>"><?php echo $genre_row["genre_naam"];?></option>
                        <?php } ?>
                    </select>
                    <div class="error">
                        <p><?php echo $genre_error;?></p>
                    </div>
                </div>
            </div>
            <div class="admin_products">
                <div class="form_names"><p>* Prijs:</p></div>
                <div class="input_field">
                    <input type="number" value="<?php echo $price?>" name="price" min="0" step="any">
                    <div class="error">
                        <p><?php echo $price_error;?></p>
                    </div>
                </div>
            </div>
            <div class="admin_products">
                <div class="form_names"><p>* Datum uitgave:</p></div>
                <div class="input_field">
                    <input type="date" name="date"  value="<?php echo $date?>">
                    <div class="error">
                        <p><?php echo $date_error;?></p>
                    </div>
                </div>
            </div>
            <div class="admin_products">
                <div class="form_names"><p>* Afbeelding:</p></div>
                <div class="input_field">
                    <input id="upload" type="file" name="image" value="<?php echo $image?>">
                    <div class="error">
                        <p><?php echo $image_error;?></p>
                    </div>
                </div>
            </div>
            <div class="admin_products">
                <div class="form_names"><p>* Omschrijving:</p></div>
                <div class="input_field">
                    <textarea name="description" rows="4" cols="60"><?php echo $description?></textarea>
                    <div class="error">
                        <p><?php echo $description_error;?></p>
                    </div>
                </div>
            </div>
            <div class="add_change_product">
                    *: verplicht veld
                    <br><br>
                    <input type="submit" value="Product toevoegen"/>
            </div>
        </form>
    </div>

    <?php include 'phpinclude/footer.php'; ?>
</body>

</html>
