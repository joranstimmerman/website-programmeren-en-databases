<!doctype html>
<html lang="en">

<head>
    <title>JEM Records | Mijn gegevens</title>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/standard_page.css">
    <link rel="stylesheet" type="text/css" href="css/navbar.css">
    <link rel="stylesheet" type="text/css" href="css/footer.css">
    <link rel="stylesheet" type="text/css" href="css/user_details.css">
    <link rel="icon" href="images/ui_icons/tabicoon.png" type="image/png">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/myScripts.js"></script>
</head>

<body>
    <?php include 'phpinclude/header.php';

        include "php/jem_queries.php";
        include "php/opendb.php";

        /* Verify authentication */
        if (isset($_SESSION['authentication']) && $_SESSION['authentication'] > 0) {

            include "php/user_detail_form.php";
        }
        else if (!isset($_SESSION['authentication']))
        {
            header("Location: 401.php");
        }
        else if ($_SESSION['authentication'] < 1)
        {
            header("Location: register_status.php");
        }
    ?>

    <div id="main_content">
        <h2>Mijn gegevens</h2>
        <div id="success_message">
            <p><?php print $success; ?></p>
        </div>
        
        <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
            <div class="details_input">
                <div class="names_form"><p>* Voornaam:</p></div>
                <div class="fields_input">
                    <input type="text" name="voornaam" value="<?php echo $first_name; ?>"/>
                    <div class="error_message">
                        <p><?php echo $fn_err;?></p>
                    </div>
                </div>
            </div>

            <div class="details_input">
                <div class="names_form"><p>  Tussenvoegsel:</p></div>
                <div class="fields_input">
                    <input type="text" name="tussenvoegsel" value="<?php echo $middle; ?>"/>
                    <div class="error_message">
                        <p><?php echo $m_err;?></p>
                    </div>
                </div>
            </div>

            <div class="details_input">
                <div class="names_form"><p>* Achternaam:</p></div>
                <div class="fields_input">
                    <input type="text" name="achternaam" value="<?php echo $last_name; ?>"/>
                    <div class="error_message">
                        <p><?php echo $ln_err;?></p>
                    </div>
                </div>
            </div>

            <div class="details_input">
                <div class="names_form"><p>* Straatnaam:</p></div>
                <div class="fields_input">
                    <input type="text" name="straatnaam" value="<?php echo $street_name; ?>"/>
                    <div class="error_message">
                        <p><?php echo $sn_err;?></p>
                    </div>
                </div>
            </div>

            <div class="details_input">
                <div class="names_form"><p>* Huisnummer:</p></div>
                <div class="fields_input">
                    <input type="number" name="huisnummer" value="<?php echo $house_number; ?>" min=1 step=1/>
                    <div class="error_message">
                        <p><?php echo $hn_err;?></p>
                    </div>
                </div>
            </div>

            <div class="details_input">
                <div class="names_form"><p>  Huisnummer toevoeging:</p></div>
                <div class="fields_input">
                    <input type="text" name="huisnummer_toevoeging" value="<?php echo $house_number_addition; ?>"/>
                    <div class="error_message">
                        <p><?php echo $hna_err;?></p>
                    </div>
                </div>
            </div>

            <div class="details_input">
                <div class="names_form"><p>* Postcode:</p></div>
                <div class="fields_input">
                    <input type="text" name="postcode" value="<?php echo $zipcode; ?>"/>
                    <div class="error_message">
                        <p><?php echo $zc_err;?></p>
                    </div>
                </div>
            </div>

            <div class="details_input">
                <div class="names_form"><p>* Stad:</p></div>
                <div class="fields_input">
                    <input type="text" name="stad" value="<?php echo $city; ?>"/>
                    <div class="error_message">
                        <p><?php echo $c_err;?></p>
                    </div>
                </div>
            </div>

            <div class="details_input">
                <div class="names_form"><p>* E-mailadres:</p></div>
                <div class="fields_input">
                    <input type="text" name="emailadres" value="<?php echo $emailaddress; ?>"/>
                    <div class="error_message">
                        <p><?php echo $e_err;?></p>
                    </div>
                </div>
            </div>

            <div class="details_save">
                <p>*: verplicht veld</p>
                <br><br>
                <input type="submit" value="Opslaan"/>
            </div>
        </form>
    </div>

    <?php include 'phpinclude/footer.php'; ?>
</body>

</html>
