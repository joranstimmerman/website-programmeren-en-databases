<!doctype html>
<html lang="en">

<head>
    <title>JEM Records | Afrekenen</title>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/standard_page.css">
    <link rel="stylesheet" type="text/css" href="css/navbar.css">
    <link rel="stylesheet" type="text/css" href="css/footer.css">
    <link rel="stylesheet" type="text/css" href="css/payment.css">
    <link rel="icon" href="images/ui_icons/tabicoon.png" type="image/png">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/myScripts.js"></script>
</head>

<body>
    <?php include 'phpinclude/header.php';

    include "php/opendb.php";
    include "php/jem_queries.php";

    /* Get paymentmethod from database */
    $get_payname_query = $db->prepare(get_payname_by_id());
    $get_payname_query->bindValue(1, $_SESSION["payment_method"], PDO::PARAM_INT);
    $get_payname_query->execute();

    $get_payname_row = $get_payname_query->fetch(PDO::FETCH_NUM);

    /* Verify authenticaton */
    if (!isset($_SESSION['authentication']) &&! $_SESSION['authentication'] > 0) {
        header("Location: 401.php");
    }
    ?>

    <div id="main_content">
        <div id="title_paypage">
            <h2>Afrekenen</h2>
            <p>Dit is de pagina van de online betaalomgeving van <?php echo $get_payname_row[0]; ?>.</p>
            <form action="<?php echo htmlspecialchars("php/user_payment.php"); ?>" method="POST">
                <input type="hidden" value="2" name="payed">
                <button type="submit">Afrekenen</button>
            </form>
            <form action="<?php echo htmlspecialchars("php/user_payment.php"); ?>" method="POST">
                <input type="hidden" value="1" name="payed">
                <button type="submit">Niet afrekenen</button>
            </form>
        </div>
    </div>

    <?php include 'phpinclude/footer.php'; ?>
</body>

</html>