<!doctype html>
<html lang="en">

<head>
    <title>JEM Records | Home</title>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/standard_page.css">
    <link rel="stylesheet" type="text/css" href="css/navbar.css">
    <link rel="stylesheet" type="text/css" href="css/footer.css">
    <link rel="stylesheet" type="text/css" href="css/home.css">
    <link rel="icon" href="images/ui_icons/tabicoon.png" type="image/png">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/myScripts.js"></script>
</head>

<body>
    <?php include 'phpinclude/header.php'; ?>

    <?php
        include "php/opendb.php";
        include "php/jem_queries.php";

        /* Get most sold and most recent albums and their information from database */
        $recent_albums = $db->query(most_recent_query());
        $most_sold_albums = $db->query(most_sold_query())
    ?>

    <div id="banner">
        <a href="product.php?album_id=100"><img src="images/album_covers/muse.jpg" alt="Banner"></a>
    </div>
    <div id="main_content">
        <div class="home_albums">
            <h2> ZOJUIST UITGEBRACHT </h2>
            <div class="home_table">
                <table>
                    <tr>
                        <?php while($row = $recent_albums->fetch(PDO::FETCH_ASSOC)) { ?>
                            <td class="home_album">
                                <a href="product.php?album_id=<?php echo $row['album_id'] ?>"><img src=" <?php echo $row['album_cover'] ?>" alt="<?php echo $row['titel']?>"><br></a>
                                <a href="product.php?album_id=<?php echo $row['album_id'] ?>">
                                    <div class="product_link">
                                        <?php echo $row['artiest'] ?><br>
                                        <?php echo $row['titel'] ?><br>
                                    </div>
                                </a>
                                &#8364; <?php echo $row['prijs'] ?>
                            </td>
                        <?php } ?>
                    </tr>
                </table>
            </div>
        </div>

        <div class="home_albums">
            <h2> MEEST VERKOCHTE ALBUMS </h2>
            <div class="home_table">
                <table>
                    <tr>
                        <?php while($row = $most_sold_albums->fetch(PDO::FETCH_ASSOC)) { ?>
                            <td class="home_album">
                                <a href="product.php?album_id=<?php echo $row['album_id'] ?>"><img src=" <?php echo $row['album_cover']?>" alt="<?php echo $row['titel']?>"></a><br>
                                <a href="product.php?album_id=<?php echo $row['album_id'] ?>">
                                    <div class="product_link">
                                        <?php echo $row['artiest'] ?><br>
                                        <?php echo $row['titel'] ?><br>
                                    </div>
                                </a>
                                &#8364; <?php echo $row['prijs'] ?>
                            </td>
                        <?php } ?>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <?php include 'phpinclude/footer.php'; ?>
</body>

</html>