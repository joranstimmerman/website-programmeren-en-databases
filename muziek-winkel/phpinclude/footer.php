<div id="footer">
    <div id="footer_content">
        <div class="footer_column">
            <div class="link">
                <a class="footer_title" href="customer_service.php">Klantenservice</a>
                <ul>
                    <li><a href="customer_service.php#contact">Contact</a></li>
                    <li><a href="customer_service.php#bestel_ontvang">Bestellen en ontvangen</a></li>
                    <li><a href="customer_service.php#retourneer">Retourneren</a></li>
                    <li><a href="customer_service.php#faq">FAQ</a></li>
                </ul>
            </div>
        </div>
        <div class="footer_column">
            <div class="link">
                <a class="footer_title" href="customer_service.php#bestel_ontvang">Makkelijk online shoppen</a>
                <ul>
                    <li><a href="customer_service.php#bestel_ontvang">Levertijd: 1-3 werkdagen</a></li>
                    <li><a href="customer_service.php#bestel_ontvang">Gratis bezorging</a></li>
                    <li><a href="customer_service.php#retourneer">Gratis retour</a></li>
                    <li><a href="customer_service.php#retourneer">21 dagen garantie</a></li>
                </ul>
            </div>
        </div>
        <div class="footer_column footer_icons">
            <div class="link">
                <a class="footer_title" href="payment_methods.php">Betaalmethoden</a>
            </div>
            <ul>
                <li><a href="payment_methods.php#ideal"><img src="images/ui_icons/ideal.png" alt="iDeal icoon"></a></li>
                <li><a href="payment_methods.php#paypal"><img src="images/ui_icons/paypal.png" alt="PayPal icoon"></a></li>
                <li><a href="payment_methods.php#creditcard"><img src="images/ui_icons/mastercard.png" alt="Mastercard icoon"></a></li>
                <li><a href="payment_methods.php#creditcard"><img src="images/ui_icons/visa.png" alt="Visa icoon"></a></li>
            </ul>

        </div>
    </div>

    <hr id="footerend">
    <p class="copyright">Copyright &copy 2019 JEM Records. All Rights Reserved.</p>
</div>