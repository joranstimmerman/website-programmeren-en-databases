<div id="header">
    <a id="link_logo" href="index.php">
        <img src="images/ui_icons/logo.png" alt="JEM Records Logo">
    </a>
    <div id="navbar">
        <div class="links1">
            <div class="links2">
                <a href="index.php" class="icons buttons"><img src="images/ui_icons/home.png" alt="Home icoon">Home</a>
                <a href="product_overview.php" class="icons buttons"><img src="images/ui_icons/product.png" alt="Producten icoon">Producten</a>


<?php
    session_start();

    /* Verify authentication and show the content based on accounttype */
    if (isset($_SESSION['user_id'])) {
        if ($_SESSION['authentication'] == 2) { ?>
                <a href="shopping_cart.php" class="icons buttons"><img src="images/ui_icons/cart.png" alt="Account icoon">Winkelmandje</a>
                <div id="ingelogd" class="dropdown">
                    <button class="dropbtn" type=" button"><img src="images/ui_icons/account.png" alt="Account icoon"> Beheer</button>
                    <div class="dropdown-content">
                        <a href="admin_accounts.php">Bestaande Accounts</a>
                        <a href="admin_orders.php">Geplaatste Bestellingen</a>
                        <a href="admin_retour.php">Geplaatste retourneringen</a>
                        <a href="administrator.php">Albums toevoegen</a>
                        <a href="user_details.php">Mijn gegevens</a>
                        <a href="user_orders.php">Mijn bestellingen</a>
                        <a href="user_retour.php">Mijn retourneringen</a>
                        <a href="customer_service.php">Klantenservice</a>
                        <a href="php/logout.php" type="button">Uitloggen</a>
                    </div>
                </div>
        <?php } else if ($_SESSION['authentication'] == 1) { ?>
                <a href="shopping_cart.php" class="icons buttons"><img src="images/ui_icons/cart.png" alt="Account icoon">Winkelmandje</a>
                <div id="ingelogd" class="dropdown">
                    <button class="dropbtn" type="button"><img src="images/ui_icons/account.png" alt="Account icoon"> Account</button>
                    <div class="dropdown-content">
                        <a href="user_orders.php">Mijn bestellingen</a>
                        <a href="user_retour.php">Mijn retourneringen</a>
                        <a href="user_details.php">Mijn gegevens</a>
                        <a href="customer_service.php">Klantenservice</a>
                        <a href="php/logout.php" type="button">Uitloggen</a>
                    </div>
                </div>
        <?php } else if ($_SESSION['authentication'] == 0) { ?>
                <div id="ingelogd" class="dropdown">
                    <button class="dropbtn" type="button"><img src="images/ui_icons/account.png" alt="Account icoon"> Account</button>
                    <div class="dropdown-content">
                        <a href="user_details.php">Mijn gegevens</a>
                        <a href="customer_service.php">Klantenservice</a>
                        <a href="php/logout.php" type="button">Uitloggen</a>
                    </div>
                </div>
        <?php } } else { ?>
            <a href="login.php" id="login" class="buttons">Inloggen</a>
        <?php } ?>
            </div>
            <div class="search_form">
                <form action="<?php echo htmlspecialchars("../product_overview.php"); ?>" method="GET">
                    <input type="text" name="search" autocomplete="off" onkeyup="getSearchResults(this.value)" placeholder="Zoek een album of artiest...">
                    <button type="submit" class="search-button"><img src="images/ui_icons/search.png" alt="Zoek icoon"></button>
                    <div id="queryResult"></div>
                </form>
            </div>
        </div>
    </div>
</div>
