<!doctype html>
<html lang="en">

<head>
    <title>JEM Records | Bestellingen beheren</title>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/standard_page.css">
    <link rel="stylesheet" type="text/css" href="css/navbar.css">
    <link rel="stylesheet" type="text/css" href="css/footer.css">
    <link rel="stylesheet" type="text/css" href="css/admin_orders.css">
    <link rel="icon" href="images/ui_icons/tabicoon.png" type="image/png">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/myScripts.js"></script>
</head>

<body>
    <?php include 'phpinclude/header.php'; ?>

    <?php
        include "php/jem_queries.php";
        include "php/opendb.php";
        include "php/find_orders_form.php";

        /* Verify authenticaton */
        if ($_SESSION['authentication'] != 2) {
            header("Location: 401.php");
        }

        /* Get orderstatus from database */
        $status_query = $db->query(get_status());
        $status_data = $status_query->fetchAll();
    ?>

    <div id="main_content">
        <h2>Bestellingen beheren</h2>
        <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" method="POST">
            <div class="search_order">
                <div class="input_names"><p>Klantnummer: </p></div>
                <div class="input_field">
                    <input type="number" name="customer_id" min ="1" step ="1"><br>
                </div>
            </div>
            <div class="search_order">
                <div class="input_names"><p>Ordernummer: </p></div>
                <div class="input_field">
                    <input type="number" name="order_id" min ="1" step ="1"><br>
                </div>
            </div>
            <div class="search_order">
                <div class="input_names"><p>Bestel datum: </p></div>
                <div class="input_field">
                    <input type="date" name="order_date"><br>
                </div>
            </div>
            <div class="search_order">
                <div class="input_names"><p>Bestelling status: </p></div>
                <div class="input_field">
                    <select name="find_status">
                        <option value=""></option>
                        <?php foreach ($status_data as $status_row) { ?>
                        <option value="<?php echo $status_row["status_id"]?>"><?php echo $status_row["status_tekst"];?></option>
                        <?php } ?>
                    </select><br>
                </div>
            </div>
            <div class="search_order_btn">
                <input type="submit" value="Vind bestellingen"><br>
            </div>
        </form>

        <div class="list_orders">
            <!-- Get orders and their information from database and check if there are results -->
            <?php if ($admin_orders->rowCount() < 1) { ?>
            <p><b>Geen bestellingen met de opgegeven gegevens gevonden.</b></p>
            <?php } ?>
            <?php foreach ($admin_orders as $admin_order_row ) { ?>
            <div class="one_order">
                <div class="img_order">
                    <h3><?php echo $admin_order_row['bestel_datum']?></h3>
                    <img src="
                    <?php
                        $first_order_albums_query = $db->prepare(get_first_user_order_albums_query());
                        $first_order_albums_query->bindValue(1, intval($admin_order_row['bestelling_id']), PDO::PARAM_INT);
                        $first_order_albums_query->execute();
                        $first_order_albums_row = $first_order_albums_query->fetch(PDO::FETCH_ASSOC);

                        echo $first_order_albums_row['album_cover'];
                    ?>" alt="album_cover">
                </div>

                <div class="info_order">
                <?php
                    $user_query = $db->prepare(get_user_query());
                    $user_query->bindValue(1, $admin_order_row['gebruiker_gebruiker_id'], PDO::PARAM_INT);
                    $user_query->execute();
                    $user_row = $user_query->fetch(PDO::FETCH_ASSOC);
                ?>
                    <h3><?php echo $admin_order_row['bestelling_id']?></h3>
                    <p>Ordernummer: <?php echo $admin_order_row['bestelling_id']?></p>
                    <p>Klantnummer: <?php echo $user_row['gebruiker_id']?></p>
                    <p>Verzendadres: <?php echo $user_row['straatnaam']; echo ' ';
                                        echo $user_row['huisnummer'];
                                        echo $user_row['huisnummer_toevoeging']; echo ' ';
                                        echo $user_row['postcode']; echo ' ';
                                        echo $user_row['plaats'];
                                    ?>
                    </p>
                    <p>Totaal: &euro;
                    <?php
                        /* Calculate total price of order */
                        $order_price_query = $db->prepare(get_order_total_query());
                        $order_price_query->bindValue(1, $admin_order_row['bestelling_id'], PDO::PARAM_INT);
                        $order_price_query->execute();
                        $order_price_row = $order_price_query->fetch(PDO::FETCH_ASSOC);

                        echo $order_price_row['order_total'];
                    ?>
                    </p>
                </div>

                <div class="info_order">
                    <h3>Producten</h3>
                    <div class="album_orders">
                        <table>
                        <?php
                            /* Get the albums corresponding with a specific order */
                            $order_albums_query = $db->prepare(get_user_order_albums_query());
                            $order_albums_query->bindValue(1, $admin_order_row['bestelling_id'], PDO::PARAM_INT);
                            $order_albums_query->execute();

                            while($order_albums_row = $order_albums_query->fetch(PDO::FETCH_ASSOC)) { ?>
                            <tr>
                                <td>
                                    <a href="product.php?album_id=<?php echo $order_albums_row['album_id'] ?>">
                                        <?php echo $order_albums_row['artiest'] ?> -
                                        <?php echo $order_albums_row['titel'] ?>
                                    </a>
                                    <span style="float:right">: <?php echo $order_albums_row['aantal'] ?></span>
                                </td>
                            </tr>
                        <?php } ?>
                        </table>
                    </div>
                </div>

                <div class="info_order">
                    <h3>Status</h3>
                    <form action="<?php echo htmlspecialchars("php/order_status.php"); ?>" method="post">
                        <input type='hidden' name='order_id_status' value=<?php echo $admin_order_row['bestelling_id']?>/>
                        <!-- Make it possible to change order status -->
                        <select name="status" class="status">
                            <option value="<?php $admin_order_row['bestelling_status_status_id']?>"><?php echo $status_data[intval($admin_order_row['bestelling_status_status_id']) - 1][1]; ?></option>
                            <?php foreach ($status_data as $status_row) {
                                if ($admin_order_row['bestelling_status_status_id'] != $status_row['status_id']) { ?>
                                    <option value="<?php echo $status_row["status_id"]?>"><?php echo $status_row["status_tekst"];?></option>
                            <?php } } ?>
                        </select><br>
                        <input id="status_btn" type = "submit" value="OK" />
                    </form>
                </div>
            </div>
            <?php  } ?>
        </div>
    </div>

    <?php include 'phpinclude/footer.php'; ?>
</body>

</html>