<!doctype html>
<html lang="en">

<head>
    <title>JEM Records | Winkelmandje</title>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/standard_page.css">
    <link rel="stylesheet" type="text/css" href="css/navbar.css">
    <link rel="stylesheet" type="text/css" href="css/footer.css">
    <link rel="stylesheet" type="text/css" href="css/shopping_cart.css">
    <link rel="icon" href="images/ui_icons/tabicoon.png" type="image/png">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/myScripts.js"></script>
</head>

<body>
    <?php
        include 'phpinclude/header.php';

        include 'php/opendb.php';
        include 'php/jem_queries.php';

        /* Verify authentication and get shopping cart from currrent user from database */
        if (isset($_SESSION['authentication']) && $_SESSION['authentication'] > 0) {
            $cur_user = intval($_SESSION['user_id']);
            $cart_query = $db->prepare(get_user_cart());

            $cart_query->bindValue(1, $cur_user, PDO::PARAM_INT);
            $cart_query->execute();
        }
        else if (!isset($_SESSION['authentication']))
        {
            header("Location: 401.php");
        }
        else if ($_SESSION['authentication'] < 1)
        {
            header("Location: register_status.php");
        }
    ?>

    <div id="main_content">
        <h2>Winkelmandje</h2>
        <!-- Check if there are results -->
        <?php if ($cart_query->rowCount() < 1) { ?>
        <div  class="empty_cart"><p>U heeft nog geen producten in uw winkelmandje.</p></div>
        <?php }
              else {
                while ($cart_row = $cart_query->fetch(PDO::FETCH_ASSOC)) { ?>
                    <div class="product">
                        <h3><?php echo $cart_row['titel'] ?></h3>
                        <div class="product_img">
                            <a href="product.php?album_id=<?php echo $cart_row['album_album_id'] ?>"><img src="<?php echo $cart_row['album_cover'] ?>" alt="<?php echo $cart_row['titel'] ?>"></a>
                        </div>

                        <div class="product_info">
                            <div class="list">
                                <p>Titel: <?php echo $cart_row['titel'] ?></p>
                                <p>Artiest: <?php echo $cart_row['artiest'] ?></p>
                                <p>Uitgave: <?php echo $cart_row['datum_uitgave'] ?></p>
                            </div>

                            <div class="amount">
                                <a style="font-size: x-large" href="php/update_quantity.php?album_id=<?php echo $cart_row['album_album_id'] ?>&amount=<?php echo intval($cart_row['aantal']) - 1?>">- </a>
                                    <?php echo $cart_row['aantal'] ?>
                                <a href="php/update_quantity.php?album_id=<?php echo $cart_row['album_album_id'] ?>&amount=<?php echo intval($cart_row['aantal']) + 1?>"> +</a>
                            </div>

                            <div class="price">
                                <!-- Calculate total price of specific album base on amount-->
                                <p>&euro; <?php echo floatval($cart_row['prijs']) * intval($cart_row['aantal']) ?> ,-</p>
                            </div>

                            <div class="delete_btn">
                                <form method="post" action="php/rem_from_cart.php?del_album_id=<?php echo $cart_row['album_album_id'] ?>">
                                    <button type="submit">Verwijder uit winkelmand</button>
                                </form>
                            </div>
                        </div>
                    </div>
        <?php   }
              } ?>
              
        <div class="bottom_cart">
            <div class="product_border">
                <div class="price_info">
                    <div class="price_text">
                        <p>Totaal:</p>
                    </div>
                    <div class="total_price">
                        <!-- Calculate total price shopping cart-->
                        <?php
                            $cart_total_query = $db->prepare(get_cart_total_query());
                            $cart_total_query->bindValue(1, $cur_user, PDO::PARAM_INT);
                            $cart_total_query->execute();
                            $cart_total_row = $cart_total_query->fetch(PDO::FETCH_NUM);
                        ?>
                        <p>&euro; <?php echo $cart_total_row[0] ?> ,-</p>
                    </div>
                </div>

                <div class="cart_btns">
                    <!-- Check if shoppingcart is empty, if so, there can not be paid-->
                    <?php if($cart_query->rowCount() > 0) { ?>
                        <form method="post" action="<?php echo htmlspecialchars("detail_check.php"); ?>">
                            <button id="pay" type="submit">Betalen</button>
                        </form>
                    <?php } ?>
                    <form method="post" action="<?php echo htmlspecialchars("product_overview.php"); ?>">
                        <button id="shop" type="submit">Verder winkelen</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <?php include 'phpinclude/footer.php'; ?>
</body>

</html>