<!doctype html>
<html lang="en">

<head>
    <title>JEM Records | Gegevenscheck en betalen</title>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/standard_page.css">
    <link rel="stylesheet" type="text/css" href="css/navbar.css">
    <link rel="stylesheet" type="text/css" href="css/footer.css">
    <link rel="stylesheet" type="text/css" href="css/detail_check.css">
    <link rel="icon" href="images/ui_icons/tabicoon.png" type="image/png">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/myScripts.js"></script>
</head>

<body>
    <?php include 'phpinclude/header.php'; ?>

    <?php

    include 'php/opendb.php';
    include 'php/jem_queries.php';

    /* Get paymentmethods from database */
    $payingmethods = $db->query(get_paymethods());

    /* Verify authenticaton and get information from current user from database */
    if (isset($_SESSION['authentication']) && $_SESSION['authentication'] > 0) {
        $cur_user = intval($_SESSION['user_id']);

        $user_details = $db->prepare(get_user_query());
        $user_details->bindValue(1, $cur_user, PDO::PARAM_INT);
        $user_details->execute();
        $user = $user_details->fetch();
    } else {
        header("Location: login.php");
    }
    ?>

    <div id="main_content">
        <div id="title_datacheckpage">
            <h2>Gegevenscheck en betalen</h2>
            <p>
                Check of uw gegevens kloppen. Zo niet?
                Pas dit dan eerst aan bij "Mijn gegevens" door <a href="user_details.php">hier</a> te klikken.
            </p>
        </div>
        <div class="data_input">
            <div class="data_names"><p>Voornaam:</p></div>
            <div class="data_input_values"><p><?php echo $user["voornaam"] == "" ? "leeg" : $user["voornaam"]; ?></p></div>
        </div>
        <div class="data_input">
            <div class="data_names"><p>Tussenvoegsel:</p></div>
            <div class="data_input_values"><p><?php echo $user["tussenvoegsel"] == "" ? "n.v.t." : $user["tussenvoegsel"]; ?></p></div>
        </div>
        <div class="data_input">
            <div class="data_names"><p>Achternaam:</p></div>
            <div class="data_input_values"><p><?php echo $user["achternaam"] == "" ? "leeg" : $user["achternaam"]; ?></p></div>
        </div>
        <div class="data_input">
            <div class="data_names"><p>Straatnaam:</p></div>
            <div class="data_input_values"><p><?php echo $user["straatnaam"] == "" ? "leeg" : $user["straatnaam"]; ?></p></div>
        </div>
        <div class="data_input">
            <div class="data_names"><p>Huisnummer:</p></div>
            <div class="data_input_values"><p><?php echo $user["huisnummer"] == "" ? "leeg" : $user["huisnummer"]; ?></p></div>
        </div>
        <div class="data_input">
            <div class="data_names"><p>Huisnummer toevoeging:</p></div>
            <div class="data_input_values"><p><?php echo $user["huisnummer_toevoeging"] == "" ? "n.v.t." : $user["huisnummer_toevoeging"]; ?></p></div>
        </div>
        <div class="data_input">
            <div class="data_names"><p>Postcode:</p></div>
            <div class="data_input_values"><p><?php echo $user["postcode"] == "" ? "leeg" : $user["postcode"]; ?></p></div>
        </div>
        <div class="data_input">
            <div class="data_names"><p>Stad:</p></div>
            <div class="data_input_values"><p><?php echo $user["plaats"] == "" ? "leeg" : $user["plaats"]; ?></p></div>
        </div>
        <div id="methodchoice">
            <p>Kies hier met welke betaalmethode je wil afrekenen:</p>
            <form action="php/check_detail_route.php" method="post">
                <!-- mae it possible to choose a paymentmethod-->
                <select name="paymethods">
                    <?php while ($row = $payingmethods->fetch(PDO::FETCH_ASSOC)) { ?>
                        <option value="<?php echo $row['betaal_methode_id'] ?>"><?php echo $row['betaal_methode_naam'] ?></option>
                    <?php } ?>
                </select>
                <br><br>
                <input type="submit" value="Betaling afronden" />
            </form>
        </div>
    </div>

    <?php include 'phpinclude/footer.php'; ?>
</body>

</html>