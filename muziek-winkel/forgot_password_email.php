<!doctype html>
<html lang="en">

<head>
    <title>JEM Records | Wachtwoord</title>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/standard_page.css">
    <link rel="stylesheet" type="text/css" href="css/navbar.css">
    <link rel="stylesheet" type="text/css" href="css/footer.css">
    <link rel="stylesheet" type="text/css" href="css/forget_password.css">
    <link rel="icon" href="images/ui_icons/tabicoon.png" type="image/png">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/myScripts.js"></script>
</head>

<body>
<?php include 'phpinclude/header.php'; ?>

<div id="main_content">
    <h2>Wachtwoord vergeten</h2>
    <p>Vul uw e-mail in en klik verzend om een wachtwoord reset link naar uw e-mail te versturen.</p>

    <?php
    include 'php/opendb.php';
    include "php/jem_queries.php";

    /* Check if form is submitted and send an e-mail to the given e-mailaddress */
    if (isset($_POST['forgot_mail_subm'])) {

        $user_mail = $_POST['email'];

        $get_userid_query = $db->prepare("SELECT gebruiker_id FROM gebruiker WHERE email=?");
        $get_userid_query->bindValue(1, $user_mail, PDO::PARAM_STR);
        $get_userid_query->execute();

        echo "<p style='color: #7c9a6e'>Er is een email verzonden naar het email adres als deze bij ons geregistreerd is</p>";

        /* Check is e-mail is registered */
        if ($get_userid_query->rowCount() > 0) {

            $get_userid_row = $get_userid_query->fetch(PDO::FETCH_NUM);

            $user_id = intval($get_userid_row[0]);

            /* 16 char long md5 string as authentication code */
            $ver_code = substr(md5(uniqid(rand(), true)), 16, 16);

            $mail_message = "Beste gebruiker van JEM Records,
                            <br><br>
                            Bij deze sturen wij uw wachtwoord reset link.
                            <br> Klik op deze link om uw wachtwoord te veranderen:
                            <a href='https://agile102.science.uva.nl/forgot_password.php?auth_code=" . $ver_code . "&user_mail=" . $user_mail . "'>
                            https://agile102.science.uva.nl/forgot_password.php?auth_code=" . $ver_code . "&user_mail=" . $user_mail . "</a>
                            <br><br>
                            Met vriendelijke groet,<br>
                            JEM-Records";

            /* Make sure e-mail will be showed in HTML */
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

            /* Check if e-mail is send correctly */
            if (mail($user_mail, "JEM-Records wachtwoord reset", $mail_message, $headers)) {
                $insert_auth_code_query = $db->prepare("UPDATE gebruiker SET wachtwoord_auth =?, wachtwoord_release=NOW() + INTERVAL 10 MINUTE WHERE gebruiker_id=?");
                $insert_auth_code_query->bindValue(1, $ver_code, PDO::PARAM_STR);
                $insert_auth_code_query->bindValue(2, $user_id, PDO::PARAM_INT);
                $insert_auth_code_query->execute();

                header("Location: password_reset_send.php");
            } else {
                header("Location: ../404.php");
            }
        }
    }

    ?>

    <form method="post">
        <label for="user_mail">E-mail:</label><br>
        <input id="user_mail" name="email" type="text"><br>
        <input type="submit" name="forgot_mail_subm">
    </form>
    <br>
</div>

<?php include 'phpinclude/footer.php'; ?>
</body>

</html>