<!doctype html>
<html lang="en">

<head>
    <title>JEM Records | Product verwijderen</title>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/standard_page.css">
    <link rel="stylesheet" type="text/css" href="css/navbar.css">
    <link rel="stylesheet" type="text/css" href="css/footer.css">
    <link rel="icon" href="images/ui_icons/tabicoon.png" type="image/png">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/myScripts.js"></script>
</head>

<body>
    <?php include 'phpinclude/header.php'; ?>

    <?php
        include 'php/opendb.php';
        include 'php/jem_queries.php';

        /* Verify authentication */
        if ($_SESSION['authentication'] != 2) {
            header("Location: 401.php");
        }

        /* Set album on "niet meer leverbaar" */
        $remove_query = $db->prepare(remove_query());
        $remove_query->bindValue(1, $_POST["album_id"], PDO::PARAM_INT);
        $remove_query->execute();


        /* Redirect to product overview after 3 seconds. */
        header( "refresh:3;url=../product_overview.php" );
    ?>

    <div id="main_content">
        <h2>PRODUCT VERWIJDERD</h2>
        <p>
            Product succesvol verwijderd. Binnen enkele seconden wordt u doorgestuurd
            naar het productoverzicht.
        </p>
    </div>
</body>

</html>
