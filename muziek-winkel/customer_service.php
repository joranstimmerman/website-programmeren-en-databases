<!doctype html>
<html lang="en">

<head>
    <title>JEM Records | Klantenservice</title>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/standard_page.css">
    <link rel="stylesheet" type="text/css" href="css/navbar.css">
    <link rel="stylesheet" type="text/css" href="css/footer.css">
    <link rel="stylesheet" type="text/css" href="css/customer_service.css">
    <link rel="icon" href="images/ui_icons/tabicoon.png" type="image/png">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/myScripts.js"></script>
</head>

<body>
    <?php include 'phpinclude/header.php'; ?>

    <div id="main_content">
        <div id="title_customerservice">
            <h2>Klantenservice</h2>
        </div>
        <div id="contact" class="service">
            <h3>Contact</h3>
            <address>
                    JEM Records bv
                    <br>
                    Science Park 904
                    <br>
                    1098 XH Amsterdam
            </address>
            <p>
                Telefoonnumer:<br>
                +31 (0)20 367 8888
                <br><br>
                E-mail:<br>
                <div class="contact_emailaddress">klantenservice@jamrecords.com</div>
            </p>
        </div>
        <div id="bestel_ontvang" class="service">
            <h3>Bestellen en ontvangen</h3>
            <p>
                Als u een product wil bestellen, moet u bij een product op het winkelmandje klikken, u wordt vervolgens doorgestuurd naar het winkelmandje, waar u er voor kan kiezen om door te winkelen
                of om te betalen. U moet wel inloggen of uzelf registreren voordat u een product kan bestellen.
                <br><br>
                Bent u klaar met winkelen? Ga dan naar uw winkelmandje. Hier kan u uw bestelling controleren en eventueel nog aanpassen. Is alles in orde? Klik dan op "Betalen" om uw bestelling
                af te ronden. Hierna wordt u doorgestuurd naar een pagina waar u uw gegevens kan checken en kan kiezen met welke betaalmethode u wil betalen. Rond vervolgens uw bestelling af via
                de online betaalomgeving van uw gekozen betaalmethode.
                <br><br>
                Wij streven naar een <strong>levertijd van 1-3 werkdagen</strong> nadat de bestelling is afgrond. Bestellingen worden bezorgd tussen 11.00 en 18.00. Als u niet thuis bent, dan wordt de bestelling afgegeven
                bij de buren. Is er vertraging bij het bezorgen van uw bestelling? Wij brengen u daarvan zo snel mogelijk op de hoogte via een e-mail. U kan de status van uw bestelling ook bekijken op de "Mijn bestellingen" pagina.
                <a class="service_link" href="user_orders.php">Klik hier</a> om naar "Mijn bestellingen" te gaan.
                <br><br>
                Heeft u uw bestelling niet of onvolledig ontvangen? Neem contact op met onze klantenservice, dan gaan we meteen op zoek! Is een product al beschadigd bij ontvangst? Of heeft u het verkeerde product ontvangen?
                Stuur het product dan retour, dan zorgen wij voor vervanging of het juiste product.
                <br><br>
                Alle bestellingen worden <strong>gratis</strong> bezorgd via PostNL. Wij leveren dan ook niet naar het buitenland.
            </p>
        </div>
        <div id="retourneer" class="service">
            <h3>Retourneren</h3>
            <p>Een product mag geretourneerd worden, maar daar zitten een paar voorwaarden aan:</p>
            <ul>
                <li>De garantie op een productis 21 dagen.</li>
                <li>Het product moet in orginele staat verkeren, dus de staat bij ontvangst. Zo niet en u retourneert het product toch, dan wordt de vergoeding 75% van de verkoopprijs.</li>
                <li>Het product moet de originele verpakking hebben.</li>
            </ul>
            <p>
                Retourneren is <strong>gratis</strong> en het product kan vervolgens afgegeven worden bij één van de PostNL locaties. Nadat wij het product of de producten ontvangen hebben storten
                wij het geld binnen 10 werkdagen terug op jouw rekening.
                <br><br>
                Wil u iets retourneren? Ga naar "Mijn bestellingen" en klikop de "Retour versturen" knop. Vervolgens vult u het formulier in en kan u het product terug sturen. <a class="service_link" href="user_orders.php">Klik hier</a> om naar "Mijn bestellingen" te gaan.
            </p>
        </div>
        <div id="faq" class="service">
            <h3>FAQ</h3>
            <p class="question">- Wat moet ik doen als het ontvangen product beschadigd is?</p>
            <p class="answer">Stuur het product retour met een beschrijving van wat er beschadigd is. Voor meer informatie, <a class="service_link" href="customer_service.php#bestel_ontvang">klik hier</a>.</p>
            <p class="question">- Wat moet ik doen als ik het verkeerde product heb ontvangen?</p>
            <p class="answer">Stuur het product retour en geef aan wat het juiste product had moeten zijn. Voor meer informatie, <a class="service_link" href="customer_service.php#bestel_ontvang">klik hier</a>.</p>
            <p class="question">- Wat is de levertijd na het bestellen?</p>
            <p class="answer">Normaal gesproken hanteren wij eenlevevrtijdvan 1-3 werkdagen. Voor meer informatie, <a class="service_link" href="customer_service.php#bestel_ontvang">klik hier</a>.</p>
            <p class="question">- Wordt er ook naar het buitenland geleverd?</p>
            <p class="answer">Nee, wij leveren niet naar het buitenland. Voor meer informatie, <a class="service_link" href="customer_service.php#bestel_ontvang">klik hier</a>.</p>
            <p class="question">- Hoe stuur ik een product retour?</p>
            <p class="answer">Ga naar Mijn bestellingen en klik op de Retour versturen knop. Voor meer informatie, <a class="service_link" href="customer_service.php#retourneer">klik hier</a>.</p>
            <p class="question">- Hoelang duurt het voordat ik terugbetaald wordt na het retourneren?</p>
            <p class="answer">Nadat wij het product of de producten ontvangen hebben storten wij het geld binnen 10 werkdagen terug op jouw rekening. Voor meer informatie, <a class="service_link" href="customer_service.php#bestel_ontvang">klik hier</a>.</p>
            <p class="question">- Hoe kan ik een klacht indienen?</p>
            <p class="answer">U kan contact met ons opnemen door een brief te schrijven naar ons hoofdkantoor, een e-mail te sturen naar onze klantenservice of onze klantenservice te bellen. Voor onze gegevens, <a class="service_link" href="customer_service.php#contact">klik hier</a>.</p>
        </div>
    </div>

    <?php include 'phpinclude/footer.php'; ?>
</body>

</html>
