<!doctype html>
<html lang="en">

<head>
    <title>JEM Records | Product</title>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/navbar.css">
    <link rel="stylesheet" type="text/css" href="css/footer.css">
    <link rel="stylesheet" type="text/css" href="css/product_page.css">
    <link rel="stylesheet" type="text/css" href="css/standard_page.css">
    <link rel="icon" href="images/ui_icons/tabicoon.png" type="image/png">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/myScripts.js"></script>
</head>

<body>
    <?php include 'phpinclude/header.php'; ?>

    <?php

    include 'php/opendb.php';
    include 'php/jem_queries.php';

    /* Get albums and their information from database */
    $cur_album = intval($_GET['album_id']);

    $product_query = $db->prepare(product_query());
    $product_query->bindValue(1, $cur_album, PDO::PARAM_STR);
    $product_query->execute();

    $product_row = $product_query->fetch(PDO::FETCH_ASSOC);

    /* Get related albums and their information from database */
    $related_product_query = $db->prepare(get_related_albums());
    $related_product_query->bindValue(1, $product_row['genre_id'], PDO::PARAM_STR);
    $related_product_query->bindValue(2, intval($product_row['album_id']), PDO::PARAM_INT);
    $related_product_query->execute();

    ?>

    <div id="main_content">
        <div class="product">
            <h2><?php echo $product_row['titel'] ?></h2>
            <div class="product_table">
                <div class="product_cover">
                    <img src="<?php echo $product_row['album_cover']?>" alt="<?php echo $product_row['titel']?>">
                </div>
                <div class="product_information">
                    <div class="product_description">
                        <?php echo $product_row['omschrijving']?>
                    </div>
                    <h5>Productinformatie:</h5>
                    <ul>
                        <li>Titel: <?php echo $product_row['titel'] ?></li>
                        <li>Artiest: <?php echo $product_row['artiest'] ?></li>
                        <li>Genre: <?php echo $product_row['genre_naam'] ?></li>
                        <li>Uitgave: <?php echo $product_row['datum_uitgave'] ?></li>
                    </ul>
                    <p><b>Prijs</b>: &euro; <?php echo $product_row['prijs']?>.-</p>
                    <?php if ($product_row["is_leverbaar"] == 0) { ?>
                        <p><b>Dit product is niet meer leverbaar</p>
                    <?php } ?>
                </div>
                <div class="product_buttons">
                <!-- Verify authenticaton and show the corresponding buttons-->
                    <?php if (isset($_SESSION['authentication']) && $_SESSION['authentication'] > 0 && $product_row["is_leverbaar"] == 1) { ?>
                        <form method="post" action="php/add_to_cart.php?add_album_id=<?php echo $product_row['album_id'] ?>">
                            <button class="buy_btn" type="submit">+ in winkelwagen</button>
                        </form>
                    <?php } ?>
                    <?php if (isset($_SESSION['authentication']) && $_SESSION['authentication'] == 2 && $product_row["is_leverbaar"] == 1) { ?>
                        <form method="post" action="<?php echo htmlspecialchars("edit_album.php?album_id=$cur_album"); ?>">
                            <input type='hidden' name='errors' value=1/>
                            <input type='hidden' name='album_id' value=<?php echo $cur_album?>/>
                            <button class="edit_button" type="submit">Wijzigen</button>
                        </form>
                        <form method="post" action="<?php echo htmlspecialchars("remove_album.php")?>">
                            <input type='hidden' name='album_id' value=<?php echo $cur_album?>/>
                            <button class="remove_button" type="submit">Verwijderen</button>
                        </form>
                    <?php } ?>
                </div>
            </div>
        </div>

        <div class="albums">
            <h3>Gerelateerd aan: <span id="related_text"><?php echo $product_row['titel'] ?></span></h3>
            <div id="related_table">
                <table>
                    <tr>
                        <?php while($related_product_row = $related_product_query->fetch(PDO::FETCH_ASSOC)) { ?>
                            <td class="album">
                                <a href="product.php?album_id=<?php echo $related_product_row['album_id'] ?>"><img src=" <?php echo $related_product_row['album_cover'] ?>" alt="<?php echo $related_product_row['titel']?>"></a><br>
                                <a href="product.php?album_id=<?php echo $related_product_row['album_id'] ?>">
                                    <?php echo $related_product_row['artiest'] ?><br>
                                    <?php echo $related_product_row['titel'] ?><br>
                                </a>
                                &euro; <?php echo $related_product_row['prijs'] ?>
                            </td>
                        <?php } ?>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <?php include 'phpinclude/footer.php'; ?>
</body>

</html>
