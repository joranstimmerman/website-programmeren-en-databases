<!doctype html>
<html lang="en">

<head>
    <title>JEM Records | 404: Pagina niet gevonden</title>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="css/errorpage.css">
    <link rel="icon" href="images/ui_icons/tabicoon.png" type="image/png">
</head>

<body>
    <div id="not_content">
        <div id="not_text">
            <h1 id="not_title">404</h1>
            <p id="not_message">De pagina die u zoekt bestaat niet. Na enkele seconden wordt u doorgestuurd naar onze homepagina.<br>
        </div>
    </div>
</body>

</html>

<?php
    /* Redirect to home after 3 seconds */
    header("refresh: 3; url=index.php");
?>