<!doctype html>
<html lang="en">

<head>
    <title>JEM Records | Succesvol </title>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/standard_page.css">
    <link rel="stylesheet" type="text/css" href="css/navbar.css">
    <link rel="stylesheet" type="text/css" href="css/footer.css">
    <link rel="icon" href="images/ui_icons/tabicoon.png" type="image/png">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/myScripts.js"></script>
</head>

<body>
    <?php include 'phpinclude/header.php'; ?>
    
    <div id="main_content">
        <?php
        /* Verify authenticaton and show based on it a message */
        if (!isset($_SESSION['user_id'])) { ?>
            <h2>Waarschuwing</h2>
            <p>Log eerst in voordat u uw mail adres verifieerd!</p>
            <?php header("refresh: 3; url=login.php"); ?>
        <?php
        }
        else if ($_SESSION['authentication'] > 0) { ?>
            <h2>Registratie compleet</h2>
            <p>Uw email adres is geverifieerd!</p>
            <?php header("refresh: 2; url=user_details.php");
        }
        else if (isset($_GET['status']) && $_GET['status'] == "exp") { ?>
            <h2>Waarschuwing</h2>
            <p>De verificatie link is verlopen, u kunt hieronder een nieuwe email laten verzenden</p>
        <?php
        }
        else if (isset($_GET['status']) && $_GET['status'] == "invalid") { ?>
            <h2>Waarschuwing</h2>
            <p>De verificatie link is niet (meer) geldig, u kunt hieronder een nieuwe code laten verzenden</p>
        <?php
        }
        else { ?>
            <h2>Registratie afmaken</h2>
            <p>Er is een email verstuurd naar uw email adres, klik op de link in de mail om uw account te verifieren</p>
        <?php
        }

        if (isset($_SESSION['user_id']) && $_SESSION['authentication'] < 1) { ?>
            <br>
            <p><b>Verificatie opnieuw versturen</b></p>
            <p>Als u de email niet ontvangen heeft of de link is verlopen klik dan <a href="php/auth_email_sender.php">hier</a> om een nieuwe te verzenden</p>
        <?php } ?>
    </div>

    <?php include 'phpinclude/footer.php'; ?>
</body>

</html>