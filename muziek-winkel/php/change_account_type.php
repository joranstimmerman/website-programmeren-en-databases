<?php
    include "jem_queries.php";
    include "opendb2.php";

    $account_type = "";

    /* If the form has been submitted, set user's account type. */
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $account_type = $_POST["account_type"];

        $account_query = $db->prepare(set_account_type());
        $account_query->bindValue(1, $account_type, PDO::PARAM_INT);
        $account_query->bindValue(2, $_POST["customer_id"], PDO::PARAM_INT);
        $account_query->execute();
    }

    header("Location: ../admin_accounts.php");
    exit();
?>