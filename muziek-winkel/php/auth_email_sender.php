<?php
    include 'opendb2.php';
    include "jem_queries.php";

    session_start();

    if (isset($_SESSION['user_id'])) {

        /* Get a user's authentication code from database. */
        $check_auth_code_query = $db->prepare(get_authentication());
        $check_auth_code_query->bindValue(1, $_SESSION['user_id'], PDO::PARAM_INT);
        $check_auth_code_query->execute();
        $check_auth_code_row = $check_auth_code_query->fetch(PDO::FETCH_NUM);

        /* If the user does not have an authentication code yet, we have to send one. */
        if ($check_auth_code_row[1] == 0) {
            $user_mail = $check_auth_code_row[2];
            $ver_code = substr(md5(uniqid(rand(), true)), 16, 16);
            $mail_message = "Beste gebruiker van JEM Records,
                    <br><br>
                    Bij deze sturen wij uw verificatie link.
                    <br> Klik op deze link om uw account te verifieren:
                    <a href='https://agile102.science.uva.nl/php/user_verificate.php?auth_code=" . $ver_code . "'>
                    https://agile102.science.uva.nl/php/user_verificate.php?auth_code=" . $ver_code . "</a>
                    <br><br>
                    Met vriendelijke groet,<br>
                    JEM-Records";
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

            /* When we mail the authentication code, we also have to set it in
             * the database.
             */
            if (mail($user_mail, "JEM-Records verificatie", $mail_message, $headers))
            {
                $insert_auth_code_query = $db->prepare(set_authentication());
                $insert_auth_code_query->bindValue(1, $ver_code, PDO::PARAM_STR);
                $insert_auth_code_query->bindValue(2, $_SESSION['user_id'], PDO::PARAM_INT);
                $insert_auth_code_query->execute();

                header("Location: ../register_status.php");
            }
            else
            {
                header("Location: ../404.php");
            }
        }
        else
        {
            header("Location: ../register_status.php");
        }
    }
    else
    {
        header("Location: ../login.php");
    }
?>