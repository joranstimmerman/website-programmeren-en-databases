<?php
    /* A script to destroy a user's session. */
    session_start();
    session_destroy();
    header("Location: ../index.php");
?>