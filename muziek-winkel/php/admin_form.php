<?php
    /* Set initial variables for new database entry and error messages. */
    $title = $artist = $genre = $image = $price = $date = $description = "";
    $uploaddir ="images/album_covers/";
    $title_error = $artist_error = $price_error = $date_error =
    $image_error = $description_error = $genre_error = "";
    $success_message = "";

    /* Sanitize form data by stripping whitespace and html code. */
    function clean_input($input) {
        $input = trim($input);
        $input = strip_tags($input);
        return $input;
    }

    /* If the form has been submitted, we check if the input fields are filled
    in. If an input field is empty, we display an error message. */
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (empty($_POST["title"])) {
            $title_error = "Titel is verplicht";
        }
        else {
            $title = clean_input($_POST["title"]);
        }

        if (empty($_POST["artist"])) {
            $artist_error = "Artiest is verplicht";
        }
        else {
            $artist = clean_input($_POST["artist"]);
        }

        if (empty($_POST["price"])) {
            $price_error = "Prijs is verplicht";
        }
        else {
            $price = clean_input($_POST["price"]);
        }

        if (empty($_POST["date"])) {
            $date_error = "Datum uitgave is verplicht";
        }
        else {
            $date = clean_input($_POST["date"]);
        }

        if ($_FILES['image']['size'] == 0) {
            $image_error = "Afbeelding is verplicht";
        }
        else {
            $image = $uploaddir . basename($_FILES['image']['name']);
            $imageFileType = strtolower(pathinfo($image,PATHINFO_EXTENSION));
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
                $image_error = "Onjuist type";
            }
        }

        if (empty($_POST["description"])) {
            $description_error = "Omschrijving is verplicht";
        }
        else {
            $description = clean_input($_POST["description"]);
        }
        
        if (empty($_POST["genre"])) {
            $genre_error = "Genre is verplicht";
        }
        else {
            $genre = $_POST["genre"];
        }

        /* If all error messages are empty, that means all fields are filled in
        and we can send the data to the database. */
        if (empty($title_error) && empty($artist_error) && empty($price_error)
        && empty($date_error) && empty($image_error)
        && empty($description_error) && empty($genre_error)) {

            $album_query = $db->prepare(add_album_query());
            $album_query->bindValue(1, $title, PDO::PARAM_STR);
            $album_query->bindValue(2, $artist, PDO::PARAM_STR);
            $album_query->bindValue(3, $price, PDO::PARAM_STR);
            $album_query->bindValue(4, $date, PDO::PARAM_STR);
            $album_query->bindValue(5, $description, PDO::PARAM_STR);
            $album_query->bindValue(6, $image, PDO::PARAM_STR);
            $album_query->bindValue(7, $genre, PDO::PARAM_INT);
            $album_query->execute();
            error_reporting(E_ALL ^ E_WARNING);
            move_uploaded_file($_FILES['image']['tmp_name'], $image);

            $title_error = $artist_error = $price_error = $date_error =
            $image_error = $description_error = $genre_error = "";
            $success_message = "Product succesvol toegevoegd.";
        }
    }
?>