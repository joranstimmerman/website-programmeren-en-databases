<?php
    /* Set initial variables for new database entry and error message. */
    $order_id = $customer_id = $order_date = $find_status = "";
    $admin_orders = "";

    /* Execute right query to get the orders, according to which input fields have been filled in. */
    $query = "SELECT * FROM bestelling ";

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (!empty($_POST['order_id'])) {
            $order_id = $_POST['order_id'];
            $query .= "WHERE bestelling_id = ?";

            $admin_orders = $db->prepare($query);
            $admin_orders->bindValue(1, $order_id, PDO::PARAM_INT);
            $admin_orders->execute();
        }
        else if(!empty($_POST['customer_id']) && !empty($_POST['order_date'])
            && !empty($_POST['find_status'])) {
            $customer_id = $_POST['customer_id'];
            $order_date = $_POST['order_date'];
            $find_status = $_POST['find_status'];
            $query .= "WHERE gebruiker_gebruiker_id = ? AND bestel_datum = ?
            AND bestelling_status_status_id = ?";

            $admin_orders = $db->prepare($query);
            $admin_orders->bindValue(1, $customer_id, PDO::PARAM_INT);
            $admin_orders->bindValue(2, $order_date, PDO::PARAM_STR);
            $admin_orders->bindValue(3, $find_status, PDO::PARAM_INT);
            $admin_orders->execute();
        }
        else if(!empty($_POST['customer_id']) && !empty($_POST['order_date'])) {
            $customer_id = $_POST['customer_id'];
            $order_date = $_POST['order_date'];
            $query .= "WHERE gebruiker_gebruiker_id = ? AND bestel_datum = ?";

            $admin_orders = $db->prepare($query);
            $admin_orders->bindValue(1, $customer_id, PDO::PARAM_INT);
            $admin_orders->bindValue(2, $order_date, PDO::PARAM_STR);
            $admin_orders->execute();
        }
        else if(!empty($_POST['customer_id']) && !empty($_POST['find_status'])) {
            $customer_id = $_POST['customer_id'];
            $find_status = $_POST['find_status'];
            $query .= "WHERE gebruiker_gebruiker_id = ? AND bestelling_status_status_id = ?
            ORDER BY bestel_datum DESC, bestelling_id DESC";

            $admin_orders = $db->prepare($query);
            $admin_orders->bindValue(1, $customer_id, PDO::PARAM_INT);
            $admin_orders->bindValue(2, $find_status, PDO::PARAM_INT);
            $admin_orders->execute();
        }
        else if(!empty($_POST['order_date']) && !empty($_POST['find_status'])) {
            $order_date = $_POST['order_date'];
            $find_status = $_POST['find_status'];
            $query .= "WHERE bestel_datum = ? AND bestelling_status_status_id = ?
            ORDER BY bestel_datum DESC, bestelling_id DESC";

            $admin_orders = $db->prepare($query);
            $admin_orders->bindValue(1, $order_date, PDO::PARAM_STR);
            $admin_orders->bindValue(2, $find_status, PDO::PARAM_INT);
            $admin_orders->execute();
        }
        else if(!empty($_POST['customer_id'])) {
            $customer_id = $_POST['customer_id'];
            $query .= "WHERE gebruiker_gebruiker_id = ?
            ORDER BY bestelling_id, bestel_datum DESC";

            $admin_orders = $db->prepare($query);
            $admin_orders->bindValue(1, $customer_id, PDO::PARAM_INT);
            $admin_orders->execute();
        }
        else if(!empty($_POST['order_date'])) {
            $order_date = $_POST['order_date'];
            $query .= "WHERE bestel_datum = ?";

            $admin_orders = $db->prepare($query);
            $admin_orders->bindValue(1, $order_date, PDO::PARAM_STR);
            $admin_orders->execute();
        }
        else if(!empty($_POST['find_status'])) {
            $find_status = $_POST['find_status'];
            $query .= "WHERE bestelling_status_status_id = ?";
            
            $admin_orders = $db->prepare($query);
            $admin_orders->bindValue(1, $find_status, PDO::PARAM_INT);
            $admin_orders->execute();
        }
        else {
            $admin_orders = $db->prepare($query);
            $admin_orders->execute();
        }
    }
    else {
        $admin_orders = $db->prepare($query);
        $admin_orders->execute();
    }
?>