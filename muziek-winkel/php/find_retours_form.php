<?php
    /* Set initial variables for new database entry and error message. */
    $retour_id = $order_id = $retour_date = $find_status = "";
    $admin_retours = "";

    /* Query to get the returns that should be selected according to which input
     * fields are filled in. String contamination is used to get the appropriate query.
     */
    $query = "SELECT * FROM retour_verzending ";

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (!empty($_POST['retour_id'])) {
            $retour_id = $_POST['retour_id'];
            $query .= "WHERE retour_id = ?";

            $admin_retours = $db->prepare($query);
            $admin_retours->bindValue(1, $retour_id, PDO::PARAM_INT);
            $admin_retours->execute();
        }
        else if(!empty($_POST['order_id']) && !empty($_POST['retour_date'])
            && !empty($_POST['find_status'])) {
            $order_id = $_POST['order_id'];
            $order_date = $_POST['retour_date'];
            $find_status = $_POST['find_status'];
            $query .= "WHERE bestelling_bestelling_id = ? AND datum = ?
            AND retour_status_status_id = ?";

            $admin_retours = $db->prepare($query);
            $admin_retours->bindValue(1, $order_id, PDO::PARAM_INT);
            $admin_retours->bindValue(2, $retour_date, PDO::PARAM_STR);
            $admin_retours->bindValue(3, $find_status, PDO::PARAM_INT);
            $admin_retours->execute();
        }
        else if(!empty($_POST['order_id']) && !empty($_POST['order_date'])) {
            $order_id = $_POST['order_id'];
            $order_date = $_POST['order_date'];
            $query .= "WHERE bestelling_bestelling_id = ? AND datum = ?";

            $admin_retours = $db->prepare($query);
            $admin_retours->bindValue(1, $order_id, PDO::PARAM_INT);
            $admin_retours->bindValue(2, $order_date, PDO::PARAM_STR);
            $admin_retours->execute();
        }
        else if(!empty($_POST['order_id']) && !empty($_POST['find_status'])) {
            $customer_id = $_POST['order_id'];
            $find_status = $_POST['find_status'];
            $query .= "WHERE bestelling_bestelling_id = ? AND retour_status_status_id = ?";

            $admin_retours = $db->prepare($query);
            $admin_retours->bindValue(1, $customer_id, PDO::PARAM_INT);
            $admin_retours->bindValue(2, $find_status, PDO::PARAM_INT);
            $admin_retours->execute();
        }
        else if(!empty($_POST['order_date']) && !empty($_POST['find_status'])) {
            $order_date = $_POST['order_date'];
            $find_status = $_POST['find_status'];
            $query .= "WHERE datum = ? AND retour_status_status_id = ?";

            $admin_retours = $db->prepare($query);
            $admin_retours->bindValue(1, $order_date, PDO::PARAM_STR);
            $admin_retours->bindValue(2, $find_status, PDO::PARAM_INT);
            $admin_retours->execute();
        }
        else if(!empty($_POST['order_id'])) {
            $order_id = $_POST['order_id'];
            $query .= "WHERE bestelling_bestelling_id = ?";

            $admin_retours = $db->prepare($query);
            $admin_retours->bindValue(1, $order_id, PDO::PARAM_INT);
            $admin_retours->execute();
        }
        else if(!empty($_POST['order_date'])) {
            $order_date = $_POST['order_date'];
            $query .= "WHERE datum = ?";

            $admin_retours = $db->prepare($query);
            $admin_retours->bindValue(1, $order_date, PDO::PARAM_STR);
            $admin_retours->execute();
        }
        else if(!empty($_POST['find_status'])) {
            $find_status = $_POST['find_status'];
            $query .= "WHERE retour_status_status_id = ?";
            
            $admin_retours = $db->prepare($query);
            $admin_retours->bindValue(1, $find_status, PDO::PARAM_STR);
            $admin_retours->execute();
        }
        else {
            $admin_retours = $db->prepare($query);
            $admin_retours->execute();
        }
    }
    else {
        $admin_retours = $db->prepare($query);
        $admin_retours->execute();
    }

?>