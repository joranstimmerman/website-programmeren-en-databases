<?php

include "opendb2.php";
include "jem_queries.php";

session_start();
/* Check if a user is logged in. */
if (isset($_SESSION['authentication']) && $_SESSION['authentication'] > 0) {
    $_SESSION['payment_method'] = $_POST["paymethods"];

    $cur_user = $_SESSION['user_id'];

    /* Get user details.  */
    $check_details_query = $db->prepare(get_user_details());
    $check_details_query->bindValue(1, $cur_user, PDO::PARAM_INT);
    $check_details_query->execute();

    $check_details_row = $check_details_query->fetch(PDO::FETCH_NUM);

    /* If a user has not filled in necessary info yet, send the user to
     * the user details page. Otherwise, send him to the payment page.
     */
    if ($check_details_row[0] == ""
        OR $check_details_row[1] == ""
        OR $check_details_row[2] == ""
        OR $check_details_row[3] == ""
        OR $check_details_row[4] == "") {
        header("Location: ../user_details.php");
    } else {
        header("Location: ../payment.php");
    }
}