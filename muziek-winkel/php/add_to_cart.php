<?php

    session_start();

    include 'opendb2.php';
    include 'jem_queries.php';

    /* Check if a user is logged in. */
    if (isset($_SESSION['authentication']) && $_SESSION['authentication'] > 0) {
        if (isset($_GET['add_album_id'])) {
            $cur_user = intval($_SESSION['user_id']);
            $cur_add_album = intval($_GET['add_album_id']);
            $quantity = 1;

            /* Check current inside of shopping cart for album that we want to add. */
            $check_cart_query = $db->prepare(check_user_cart());
            $check_cart_query->bindValue(1, $cur_user, PDO::PARAM_INT);
            $check_cart_query->bindValue(2, $cur_add_album, PDO::PARAM_INT);
            $check_cart_query->execute();

            /* If the same album is already in the cart we increase the quantity.
                * Otherwise we add a new album to the cart.
                */
            if ($check_cart_query->rowCount() > 0) {
                $check_cart_row = $check_cart_query->fetch(PDO::FETCH_ASSOC);
                $quantity = intval($check_cart_row['aantal']) + 1;

                $update_cart_query = $db->prepare(update_cart_product());
                $update_cart_query->bindValue(1, $quantity, PDO::PARAM_INT);
                $update_cart_query->bindValue(2, $cur_user, PDO::PARAM_INT);
                $update_cart_query->bindValue(3, $cur_add_album, PDO::PARAM_INT);
                $update_cart_query->execute();
            } else {
                $add_cart_query = $db->prepare(add_album_cart());
                $add_cart_query->bindValue(1, $cur_user, PDO::PARAM_INT);
                $add_cart_query->bindValue(2, $cur_add_album, PDO::PARAM_INT);
                $add_cart_query->bindValue(3, $quantity, PDO::PARAM_INT);
                $add_cart_query->execute();
            }

            header("Location: ../shopping_cart.php");
        } else {
            header("Location: ../index.php");
        }
    }
?>