<?php
    /* Get current album ID of product that we want to edit. */
    $cur_album = intval($_GET['album_id']);

    /* Get the products data. */
    $edit_product_data = $db->prepare(product_query());
    $edit_product_data->bindValue(1, $cur_album, PDO::PARAM_STR);
    $edit_product_data->execute();
    $edit_product_row = $edit_product_data->fetch(PDO::FETCH_ASSOC);

    /* Set path that images need to get uploaded to. */
    $uploaddir ="images/album_covers/";

    /* Set initial product values from database. */
    $title = $edit_product_row["titel"];
    $artist = $edit_product_row["artiest"];
    $price = $edit_product_row["prijs"];
    $date = $edit_product_row["datum_uitgave"];
    $image = $edit_product_row["album_cover"];
    $description = $edit_product_row["omschrijving"];
    $genre = $edit_product_row["genre_genre_id"];

    /* Declare error and success messages. */
    $title_error = $artist_error = $price_error = $date_error = $image_error =
    $description_error = $genre_error = "";
    $success_message = "";


    /* Sanitize form data by stripping whitespace and html code. */
    function clean_input($input) {
        $input = trim($input);
        $input = strip_tags($input);
        return $input;
    }

    /* If the form has been submitted, we check if the input fields are filled
    in. If an input field is empty, we display an error message. */
    if ($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["errors"] == 0) {
        if (empty($_POST["title"])) {
            $title_error = "Titel is verplicht";
        }
        else {
            $title = clean_input($_POST["title"]);
        }

        if (empty($_POST["artist"])) {
            $artist_error = "Artiest is verplicht";
        }
        else {
            $artist = clean_input($_POST["artist"]);
        }

        if (empty($_POST["price"])) {
            $price_error = "Prijs is verplicht";
        }
        else {
            $price = clean_input($_POST["price"]);
        }

        if (empty($_POST["date"])) {
            $date_error = "Datum uitgave is verplicht";
        }
        else {
            $date = clean_input($_POST["date"]);
        }

        /* If no image was uploaded, set no_image_query to 1. In this case we will
         * update the product with the old image. This is because the image can't
         * be preselected because an image upload wants you to select a local file.
         */
        if ($_FILES['image']['size'] == 0) {
            $no_image_query = 1;
        }
        else {
            /* Check if the uploaded file is really an image. */
            $image = $uploaddir . basename($_FILES['image']['name']);
            $imageFileType = strtolower(pathinfo($image,PATHINFO_EXTENSION));
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
                $image_error = "Onjuist bestand";
            }
            $no_image_query = 0;
        }

        if (empty($_POST["description"])) {
            $description_error = "Omschrijving is verplicht";
        }
        else {
            $description = clean_input($_POST["description"]);
        }

        if (empty($_POST["genre"])) {
            $genre_error = "Genre is verplicht";
        }
        else {
            $genre = $_POST["genre"];
        }

        /* If all error messages are empty, that means all fields are filled in
        and we can send the data to the database. */
        if (empty($title_error)
            && empty($artist_error)
            && empty($price_error)
            && empty($date_error)
            && empty($image_error)
            && empty($description_error)
            && empty($genre_error)) {

            /* Query to update the album data if a new image has been uploaded. */
            if ($no_image_query == 0) {
                $album_query = $db->prepare(edit_album_query());
                $album_query->bindValue(1, $title, PDO::PARAM_STR);
                $album_query->bindValue(2, $artist, PDO::PARAM_STR);
                $album_query->bindValue(3, $price, PDO::PARAM_STR);
                $album_query->bindValue(4, $date, PDO::PARAM_STR);
                $album_query->bindValue(5, $description, PDO::PARAM_STR);
                $album_query->bindValue(6, $image, PDO::PARAM_STR);
                $album_query->bindValue(7, $genre, PDO::PARAM_INT);
                $album_query->bindValue(8, $cur_album, PDO::PARAM_INT);
                $album_query->execute();
                error_reporting(E_ALL ^ E_WARNING);
                move_uploaded_file($_FILES['image']['tmp_name'], $image);
            }

            /* Query to update the album data if a new image has not been uploaded. */
            else {
                $album_query = $db->prepare(no_image_edit());
                $album_query->bindValue(1, $title, PDO::PARAM_STR);
                $album_query->bindValue(2, $artist, PDO::PARAM_STR);
                $album_query->bindValue(3, $price, PDO::PARAM_STR);
                $album_query->bindValue(4, $date, PDO::PARAM_STR);
                $album_query->bindValue(5, $description, PDO::PARAM_STR);
                $album_query->bindValue(6, $genre, PDO::PARAM_INT);
                $album_query->bindValue(7, $cur_album, PDO::PARAM_INT);
                $album_query->execute();
            }

            $success_message = "Product succesvol gewijzigd.";
        }
    }
?>