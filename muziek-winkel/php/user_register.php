<?php

    include 'opendb2.php';
    include "jem_queries.php";

    session_start();

    /* Check if there is already an account with the given e-mailaddress. */
    $check_duplicate_query = $db->prepare(check_duplicate());
    $check_duplicate_query->bindValue(1, $_POST['emailaddress'], PDO::PARAM_STR);
    $check_duplicate_query->execute();


    $check_duplicate_row = $check_duplicate_query->fetch(PDO::FETCH_NUM);

    /* Initialize variables to the values in the database. */
    $emailaddress = $password = $repeat_password = $_SESSION['register_error'] = $_SESSION['register_success'] = "";

    /* A function to clean up user input. */
    function clean_input($input) {
        $input = trim($input);
        $input = strip_tags($input);
        return $input;
    }

    /*
        Check whether the form has been submitted and give an error message if a required field is empty or
        when the input is not valid.
    */
    if (empty($_POST["emailaddress"])) {
        $_SESSION['register_error'] = '<p class="error">E-mail is een verplicht veld.</p>';
        header("Location: ../login.php");
    }
    else {
        $emailaddress = clean_input($_POST["emailaddress"]);

        if (filter_var($emailaddress, FILTER_VALIDATE_EMAIL) === FALSE) {
            $_SESSION['register_error'] = '<p class="error">Ongeldig e-mailadres. Het moet een @ en een . bevatten.</p>';
            header("Location: ../login.php");
        }
        else {
            if ($check_duplicate_row) {
                $_SESSION['register_error'] ="<p class='error'>Er bestaat al een account met dit e-mailadres.</p>";
                header("Location: ../login.php");
            }
            else {
                if (empty($_POST["password"])) {
                    $_SESSION['register_error'] = '<p class="error">Wachtwoord is een verplicht veld.</p>';
                    header("Location: ../login.php");
                }
                else {
                    $password = clean_input($_POST["password"]);

                    if (!preg_match("/^\S*(?=\S{6,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$/", $password)) {
                        $_SESSION['register_error'] = '<p class="error">Wachtwoord mag alleen letters en getallen bevatten en minimaal beschikken over minimaal 6 karakters, 1 getal een hoofdletter en een kleine letter.</p>';
                        header("Location: ../login.php");
                    }
                    else {
                        if (empty($_POST["sec_password"])) {
                            $_SESSION['register_error'] = '<p class="error">Herhaal wachtwoord is een verplicht veld.</p>';
                            header("Location: ../login.php");
                        }
                        else {
                            $repeat_password = clean_input($_POST["sec_password"]);

                            if ($password !== $_POST["sec_password"]) {
                                $_SESSION['register_error'] = '<p class="error">Wachtwoord komt niet overeen.</p>';
                                header("Location: ../login.php");
                            }
                            else {
                                /*Check that all required fields are set without error.*/
                                if (empty($_SESSION['register_error'])) {
                                    $pass_hash = password_hash($password, PASSWORD_DEFAULT);
                                    $insert_user_query = $db->prepare(insert_user());
                                    $insert_user_query->bindValue(1, $emailaddress, PDO::PARAM_STR);
                                    $insert_user_query->bindValue(2, $pass_hash, PDO::PARAM_STR);
                                    $insert_user_query->execute();

                                    /* User registered */
                                    $check_duplicate_query->execute();
                                    $user_id_row = $check_duplicate_query->fetch(PDO::FETCH_NUM);
                                    $_SESSION['user_id'] = $user_id_row[0];
                                    $_SESSION['authentication'] = $user_id_row[2];
                                    $emailaddress = $password = $repeat_password = "";

                                    header("Location: auth_email_sender.php");
                                }
                                else {
                                    /*The form has not been submitted yet.*/
                                    $emailaddress = $password = $repeat_password = "";
                                }
                            }
                        }
                    }
                }
            }
        }
    }
?>