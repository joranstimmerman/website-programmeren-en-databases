<?php
    $cur_user = intval($_SESSION['user_id']);

    /* Get user info. */
    $user_detail_query = $db->prepare(get_user_account_id());

    $user_detail_query->bindValue(1, $cur_user, PDO::PARAM_INT);
    $user_detail_query->execute();
    $user_detail_row = $user_detail_query->fetch(PDO::FETCH_ASSOC);

    /* Initialize variables to the values in the database. */
    $first_name = $user_detail_row["voornaam"];
    $middle = $user_detail_row["tussenvoegsel"];
    $last_name = $user_detail_row["achternaam"];
    $street_name = $user_detail_row["straatnaam"];
    $house_number = $user_detail_row["huisnummer"];
    $house_number_addition = $user_detail_row["huisnummer_toevoeging"];
    $zipcode = $user_detail_row["postcode"];
    $city = $user_detail_row["plaats"];
    $emailaddress = $user_detail_row["email"];

    $min = 1;
    $success = "";

    /* Clear the error strings. */
    $fn_err = $m_err = $ln_err = $sn_err = $hn_err = $hna_err = $zc_err = $c_err = $e_err = "";

    /* A function to clean up user input. */
    function clean_input($input) {
        $input = trim($input);
        $input = strip_tags($input);
        return $input;
    }

    /* Check whether the form has been submitted and give a error message if a required field is empty or
        when the input is not valid. */
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (empty($_POST["voornaam"])) {
            $fn_err = '<div class="error">Voornaam is een verplicht veld</div>';
        }
        else {
            $first_name = clean_input($_POST["voornaam"]);

            if (!preg_match("/^[a-zA-Z -]+$/", $first_name)) {
                $fn_err = '<div class="error">Voornaam mag alleen letters, spaties en - bevatten</div>';
            }
        }
        $middle = clean_input($_POST["tussenvoegsel"]);

        if (!empty($_POST['tussenvoegsel']) && !preg_match("/^[a-zA-Z -]+$/", $middle)) {
            $m_err = '<div class="error">Tussenvoegsel mag alleen letters, spaties en - bevatten</div>';
        }
        if (empty($_POST["achternaam"])) {
            $ln_err = '<div class="error">Achternaam is een verplicht veld</div>';
        }
        else {
            $last_name = clean_input($_POST["achternaam"]);

            if (!preg_match("/^[a-zA-Z -]+$/", $last_name)) {
                $ln_err = '<div class="error">Achternaam mag alleen letters, spaties en - bevatten</div>';
            }
        }

        if (empty($_POST["straatnaam"])) {
            $sn_err = '<div class="error">Straatnaam is een verplicht veld</div>';
        }
        else {
            $street_name = clean_input($_POST["straatnaam"]);

            if (!preg_match("/^[a-zA-Z -']+$/", $street_name)) {
                $sn_err = '<div class="error">Straatnaam mag alleen letters, spaties, - en \' bevatten</div>';
            }
        }

        if (empty($_POST["huisnummer"])) {
            $hn_err = '<div class="error">Huisnummer is een verplicht veld</div>';
        }
        else {
            $house_number = clean_input($_POST["huisnummer"]);

            if (!preg_match("/^([1-9][0-9]*)$/", $house_number)) {
                $hn_err = '<div class="error">Huisnummer mag alleen getallen bevatten</div>';
            }
        }
        $house_number_addition = clean_input($_POST["huisnummer_toevoeging"]);
        if (!empty($_POST['huisnummer_toevoeging']) && !preg_match("/^[a-zA-Z0-9]$/", $house_number_addition)) {
            $hna_err = '<div class="error">Huisnummer toevoeging mag alleen letters en cijfers bevatten</div>';
        }
        if (empty($_POST["postcode"])) {
            $zc_err = '<div class="error">Postcode is een verplicht veld</div>';
        }
        else {
            $zipcode = clean_input($_POST["postcode"]);

            if (!preg_match("/^([1-9][0-9]{3}[a-zA-Z]{2})$/", $zipcode)) {
                $zc_err = '<div class="error">Postcode mag alleen 4 cijfers en 2 letters bevatten, geen spaties.</div>';
            }
        }

        if (empty($_POST["stad"])) {
            $c_err = '<div class="error">Stad is een verplicht veld</div>';
        }
        else {
            $city = clean_input($_POST["stad"]);

            if (!preg_match("/^[a-zA-Z -']+$/", $city)) {
                $c_err = '<div class="error">Stad mag alleen letters, spaties, - en \' bevatten</div>';
            }
        }

        if (empty($_POST["emailadres"])) {
            $e_err = '<div class="error">E-mailadres is een verplicht veld</div>';
        }
        else {
            $emailaddress = clean_input($_POST["emailadres"]);

            if (filter_var($emailaddress, FILTER_VALIDATE_EMAIL) === FALSE) {
                $e_err = '<div class="error">Ongeldig e-mailadres</div>';
            }
        }

        /*Check that all required fields are set without error.*/
        if (empty($fn_err) && empty($ln_err) && empty($sn_err) && empty($hn_err) && empty($zc_err) && empty($c_err) && empty($e_err) && empty($p_err) && empty($rp_err)) {
            /*Add user_details to database*/
            $stmt = $db->prepare(add_customer_query());
            $stmt->bindValue(1, $first_name, PDO::PARAM_STR);
            $stmt->bindValue(2, $middle, PDO::PARAM_STR);
            $stmt->bindValue(3, $last_name, PDO::PARAM_STR);
            $stmt->bindValue(4, $street_name, PDO::PARAM_STR);
            $stmt->bindValue(5, $house_number, PDO::PARAM_INT);
            $stmt->bindValue(6, $house_number_addition, PDO::PARAM_STR);
            $stmt->bindValue(7, $zipcode, PDO::PARAM_STR);
            $stmt->bindValue(8, $city, PDO::PARAM_STR);
            $stmt->bindValue(9, $emailaddress, PDO::PARAM_STR);
            $stmt->bindValue(10, $_SESSION['user_id'], PDO::PARAM_STR);
            $stmt->execute();

            $success = "Wijzigingen opgeslagen!";
        }
    }
?>