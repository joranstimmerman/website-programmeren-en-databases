<?php
    include 'opendb2.php';
    include 'jem_queries.php';

    $search_value = "";
    $seach_value = $_GET['search_value'];

    /* Sanitize user's search input. */
    if ($seach_value != "") {
        $search_value = trim($search_value);
        $search_value = strip_tags($search_value);
    }

    /* Get results from search input. */
    $search_results = $db->prepare(search_query());
    $search_results->bindValue(1, $seach_value . "%", PDO::PARAM_STR);
    $search_results->bindValue(2, $seach_value . "%", PDO::PARAM_STR);
    $search_results->execute();
?>

<ul>
    <?php while($row = $search_results->fetch(PDO::FETCH_ASSOC)) { ?>
        <li><a href="product.php?album_id=<?php echo $row['album_id'] ?>">
        <?php echo $row['titel'] ?> - <?php echo $row['artiest'] ?><br></a></li>
    <?php } ?>
</ul>