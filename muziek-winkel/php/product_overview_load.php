<?php
    include 'opendb2.php';

    $min = $max = $sort = $genre= "";
    $min_error = $max_error = "";

    /* Check which filter options have been clicked. */
    if (isset($_GET['min_value'])) {
        $min = $_GET['min_value'];
    }

    if (isset($_GET['max_value'])) {
        $max = $_GET['max_value'];
    }

    if (isset($_GET['genre'])) {
        $genre = $_GET['genre'];
    }

    if (isset($_GET['sort'])) {
        $sort = $_GET['sort'];
    }

    /* Get the right results, according to which filters have been clicked. */
    $query = "SELECT album_cover, titel, artiest, prijs, album_id FROM album WHERE ";

    if ($max != "" && $min != "") {
        $query .= "prijs BETWEEN $min AND $max ";
    }
    else if ($max == "" && $min != "") {
        $query .= "prijs BETWEEN $min AND 100000 ";
    }
    else if ($max != "" && $min == "") {
        $query .= "prijs BETWEEN 0 AND $max ";
    }
    else {
        $query .= "prijs BETWEEN 0 AND 100000 ";
    }

    if ($genre != "") {
        $query .= "AND genre_genre_id = $genre ";
    }

    if (!empty($_GET['search_value'])) {
        $query .="AND (titel LIKE ?) OR (artiest LIKE ?) ";
    }

    /* Order the results according to which sort option the user clicked. */
    if ($sort == "titel_AZ") {
        $query .= "ORDER BY titel ASC";
    }
    elseif ($sort == "titel_ZA") {
        $query .= "ORDER BY titel DESC";
    }
    elseif ($sort == "artiest_AZ") {
        $query .= "ORDER BY artiest ASC";
    }
    elseif ($sort == "artiest_ZA") {
        $query .= "ORDER BY artiest DESC";
    }
    elseif ($sort == "prijshooglaag") {
        $query .= "ORDER BY prijs DESC";
    }
    elseif ($sort == "prijslaaghoog") {
        $query .= "ORDER BY prijs ASC";
    }


    $albums = $db->prepare($query);
    $search_value = "";

    /* Get search value. */
    if (isset($_GET['search_value'])) {
        $search_value = $_GET['search_value'];
        $albums->bindValue(1, $search_value. "%", PDO::PARAM_STR);
        $albums->bindValue(2, $search_value. "%", PDO::PARAM_STR);
    }

    $albums->execute();

    /* If no results have been found, print this. Otherwise, show the results. */
    if ($albums->rowCount() == 0) { ?>
        <p>Geen Zoekresultaat</p>
        <p>Helaas, wij vonden geen producten voor jou zoekopdracht: <?php echo $search_value ?> </p>
    <?php
    } else {
        while ($row = $albums->fetch(PDO::FETCH_ASSOC)) { ?>
            <div class="product_div">
                <a href="product.php?album_id=<?php echo $row['album_id'] ?>">
                    <img src=" <?php echo $row['album_cover'] ?>" alt="<?php echo $row['titel']?>"><br>
                </a>
                <a href="product.php?album_id=<?php echo $row['album_id'] ?>">
                    <div class="product_div_link">
                        <?php echo $row['artiest'] ?><br>
                        <?php echo $row['titel'] ?><br>
                    </div>
                </a>
                &#8364; <?php echo $row['prijs'] ?>
            </div>
<?php
        }
    }
?>
