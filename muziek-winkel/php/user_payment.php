<?php
    /* Payment of an order. */

    include "jem_queries.php";
    include "opendb2.php";

    session_start();

    /* Check if a user is logged in. */
    if (isset($_SESSION['authentication']) && $_SESSION['authentication'] > 0) {
        $cur_user = $_SESSION['user_id'];

        /* Get all products and their quantities from a user's shopping cart. */
        $get_cart_query = $db->prepare(get_cart());
        $get_cart_query->bindValue(1, $cur_user, PDO::PARAM_INT);
        $get_cart_query->execute();

        /* If there are no products in the shopping cart, the order has been placed
        * and we direct the user to their order overview. Otherwise, we set up for
        * the placement of the order.
        */
        if ($get_cart_query->rowCount() < 1)
        {
            header("Location: ../user_orders.php");
        }
        else {
            /* Check if the user payed or cancelled the order, this decides the order status. */
            $pay_status = $_POST['payed'];

            /* Get the highest order ID so we know what order ID the new order will get.
            * We will need this for the bestelling_has_album table that can have
            * duplicate order IDs for different albums in one order.
            */
            $get_highest_id_query = $db->prepare(get_highest_order_id());
            $get_highest_id_query->execute();
            $get_highest_id_row = $get_highest_id_query->fetch(PDO::FETCH_NUM);

            if ($get_highest_id_row) {
                $cur_bestelling_id = $get_highest_id_row[0] + 1;
            } else {
                $cur_bestelling_id = 1;
            }

            /* Place a new order. */
            $insert_order_query = $db->prepare(place_order());
            $insert_order_query->bindValue(1, $cur_bestelling_id, PDO::PARAM_INT);
            $insert_order_query->bindValue(2, $pay_status, PDO::PARAM_INT);
            $insert_order_query->bindValue(3, $_SESSION['payment_method'], PDO::PARAM_INT);
            $insert_order_query->bindValue(4, $cur_user, PDO::PARAM_INT);
            $insert_order_query->execute();

            /* Place all items from the shopping cart into the database, connecting it to the order. */
            while ($get_cart_row = $get_cart_query->fetch(PDO::FETCH_NUM)) {
                $get_album_query = $db->prepare(insert_order_albums());
                $get_album_query->bindValue(1, $cur_bestelling_id, PDO::PARAM_INT);
                $get_album_query->bindValue(2, $get_cart_row[0], PDO::PARAM_INT);
                $get_album_query->bindValue(3, $get_cart_row[1], PDO::PARAM_INT);
                $get_album_query->execute();
            }

            /* Empty shopping cart after order is placed. */
            $rem_cart_query = $db->prepare(empty_cart());
            $rem_cart_query->bindValue(1, $cur_user, PDO::PARAM_INT);
            $rem_cart_query->execute();

            /* Redirect to order_overview or order_cancel, depending on if the user paid or not. */
            if ($pay_status == 2) {
                header("Location: ../order_overview.php");
            } else {
                header("Location: ../order_cancel.php");
            }
        }
    }
    else
    {
        header("Location: ../401.php");
    }
?>