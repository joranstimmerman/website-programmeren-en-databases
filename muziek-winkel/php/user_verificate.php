<?php
    include 'opendb2.php';
    include "jem_queries.php";

    session_start();

    /* Check if a user is logged in. */
    if (isset($_SESSION['user_id']) && isset($_GET['auth_code'])) {

        /* Get verification details of a user. */
        $check_ver_query = $db->prepare(get_verification_details());
        $check_ver_query->bindValue(1, $_SESSION['user_id'], PDO::PARAM_STR);
        $check_ver_query->execute();

        $check_ver_row = $check_ver_query->fetch(PDO::FETCH_NUM);

        /* Check if a user has been verified. */
        if ($check_ver_row[0] > 0) {
            header("Location: ../register_status.php");
        }
        else
        {
            /* If the user has not been verified, check if the authentication link
            * has expired.
            */
            if (strtotime(date($check_ver_row[2])) > strtotime(date('Y-m-d H:i:s'))) {
                /* Check if the right authentication code has bee used. */
                if ($check_ver_row[1] == $_GET['auth_code']) {
                    /* Update account type and verification. */
                    $set_ver_query = $db->prepare(set_verification());
                    $set_ver_query->bindValue(1, $_SESSION['user_id'], PDO::PARAM_STR);
                    $set_ver_query->execute();

                    $check_ver_query->execute();

                    $get_ver_row = $check_ver_query->fetch(PDO::FETCH_NUM);

                    $_SESSION['authentication'] = $get_ver_row[0];

                    header("Location: ../register_status.php");
                } else {
                /* Show page if the verification did not succeed. */
                    header("Location: ../register_status.php?status=invalid");
                }
            }
            else
            {
                /* Show page if the link has expired, to get a new one. */
                header("Location: ../register_status.php?status=exp");
            }
        }
    }
    else
    {
        /* Show page that tells a user he is not logged in. */
        header("Location: ../register_status.php");
    }
?>