<?php
/* A form that a user can use to report a product he is returning. */

    include 'opendb2.php';
    include 'jem_queries.php';

    $order_number = $_GET['order'];

    /* Get the products from an order. */
    $bestelling_albums = $db->prepare(get_user_order_albums_query());
    $bestelling_albums->bindValue(1, intval($order_number), PDO::PARAM_INT);
    $bestelling_albums->execute();

    /* Create an array that holds the quantity of a product that is getting returned. */
    $number_returned_per_album = array();

    while($row = $bestelling_albums->fetch(PDO::FETCH_ASSOC)) {
        $number_returned_per_album[] = $_POST[$row["album_id"]];
    }

    $album_selected = 0;

    /* If at least one item of a certain album is being returned, we select the album. */
    foreach ($number_returned_per_album as $value) {
        if ($value > 0) {
            $album_selected = 1;
        }
    }

    /* Make sure a product and a reason for return is selected. */
    if ($album_selected == 0) {
        $error_msg = "<p class='error'>Er is geen reden of product(en) gekozen</p>";
    }

    if (isset($_POST['reden'])) {
        $retour_reason = $_POST['reden'];
    }
    else {
        $error_msg = "<p class='error'>Er is geen reden of product(en) gekozen</p>";
    }

    /* Show error message if no product or reason for return was selected. */
    if ($error_msg != "") {
        header("Location: ../return_product.php?order=$order_number&error=$error_msg");
        exit();
    }

    $description = $_POST['opmerking'];

    /* Insert a return into the database. */
    $insert_retour_query = $db->prepare(insert_return());
    $insert_retour_query->bindValue(1, $description, PDO::PARAM_STR);
    $insert_retour_query->bindValue(2, $order_number, PDO::PARAM_INT);
    $insert_retour_query->bindValue(3, $retour_reason, PDO::PARAM_INT);
    $insert_retour_query->execute();

    /* Get the highest return ID so we know what return ID the new return will get.
     * We will need this for the album_has_retour_verzending table that can have
     * duplicate return IDs for different albums in one return.
     */
    $get_highest_id_query = $db->prepare(get_highest_return_id());
    $get_highest_id_query->execute();

    $get_highest_id_row = $get_highest_id_query->fetch(PDO::FETCH_NUM);

    if ($get_highest_id_row)
    {
        $cur_retour_id = $get_highest_id_row[0];
    }
    else
    {
        $cur_retour_id = 1;
    }

    /* Get the products of the order. */
    $bestelling_albums2 = $db->prepare(get_user_order_albums_query());
    $bestelling_albums2->bindValue(1, intval($order_number), PDO::PARAM_INT);
    $bestelling_albums2->execute();


    /* Insert the products that are returned in the database, with the corresponding return ID. */
    $album_index = 0;
    while ($row = $bestelling_albums2->fetch(PDO::FETCH_ASSOC))
    {
        $retour_album_aantal = $db->prepare(insert_returned_albums());
        $retour_album_aantal->bindValue(1, $row['album_id'], PDO::PARAM_INT);
        $retour_album_aantal->bindValue(2, $cur_retour_id, PDO::PARAM_INT);
        $retour_album_aantal->bindValue(3, $number_returned_per_album[$album_index], PDO::PARAM_INT);
        $retour_album_aantal->execute();
        $album_index++;
    }

    /* Direct user to their overview of returned products. */
    header("Location: ../user_retour.php");
?>

