<?php
    include "jem_queries.php";
    include "opendb2.php";

    $status = "";

    /* If the form has been submitted, set order status. */
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $status = $_POST["status"];

        $status_query = $db->prepare(set_order_status());
        $status_query->bindValue(1, $status, PDO::PARAM_INT);
        $status_query->bindValue(2, $_POST["order_id_status"], PDO::PARAM_INT);
        $status_query->execute();
    }

    header("Location: ../admin_orders.php");
?>