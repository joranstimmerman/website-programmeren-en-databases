<?php
    session_start();

    include 'jem_queries.php';
    include 'opendb2.php';

    $user_logged_in = false;

    /* Get a user's login details by the given e-mail addresss. */
    $login_query = $db->prepare(user_login());
    $login_query->bindValue(1, $_POST['email'], PDO::PARAM_STR);
    $login_query->execute();

    $login_query_row = $login_query->fetch(PDO::FETCH_NUM);

    $_SESSION['inlog_error'] = "";

    /* Check if an account was found with the given e-mail address. */
    if ($login_query_row) {
        /* Check password. */
        if (password_verify($_POST['password'], $login_query_row[1]))
        {
            $user_logged_in = true;

            /* Check if a password needs re-hashing. */
            if (password_needs_rehash($login_query_row[1], PASSWORD_DEFAULT)) {
                /* Update hashed password. */
                $new_password = password_hash($_POST['password'], PASSWORD_DEFAULT);
                $update_password_query = $db->prepare('UPDATE gebruiker SET wachtwoord=? WHERE email=?');
                $update_password_query->bindValue(1, $new_password, PDO::PARAM_STR);
                $update_password_query->bindValue(2, $_POST['email'], PDO::PARAM_STR);
                $update_password_query->execute();
            }
        }
    }

    /* Check if user is logged in. */
    if ($user_logged_in) {
        /* Check the user's accounttype. */
        $account_type = $db->prepare(user_account_type());
        $account_type->bindValue(1, $_POST['email'], PDO::PARAM_STR);
        $account_type->execute();

        $account_type_row = $account_type->fetch(PDO::FETCH_NUM);

        /* Set sessions with account type and user ID. */
        $_SESSION['authentication'] = $account_type_row[0];
        $_SESSION['user_id'] = $login_query_row[0];
        /* User logged in */
        header("Location: ../index.php");
    }
    else {
        $email = $_POST['email'];
        $_SESSION['inlog_error'] ="<p class='error'>De combinatie van e-mailadres en wachtwoord is niet geldig.</p>";
        header("Location: ../login.php");
    }
?>