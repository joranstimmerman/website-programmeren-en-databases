<?php
    /* Set initial variables for new database entry and error message. */
    $customer_id = $last_name = "";
    $admin_accounts = "";

    /* Get the right accounts, according to which input fields are filled in. */
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (!empty($_POST['customer_id'])) {
            $customer_id = $_POST['customer_id'];
            $admin_accounts = $db->prepare(get_user_account_id());
            $admin_accounts->bindValue(1, $customer_id, PDO::PARAM_INT);
            $admin_accounts->execute();
        }
        else if(!empty($_POST['last_name'])) {
            $last_name = $_POST['last_name'];
            $admin_accounts = $db->prepare(get_user_account_name());
            $admin_accounts->bindValue(1, $last_name, PDO::PARAM_STR);
            $admin_accounts->execute();
        }
        else {
            $admin_accounts = $db->prepare(get_all_users());
            $admin_accounts->execute();
        }
    }
    else {
        $admin_accounts = $db->prepare(get_all_users());
        $admin_accounts->execute();
    }
?>