<?php
    /* Updating the quantity of a product in the shopping cart. */
    session_start();

    include "opendb2.php";

    /* Check if a user is logged in. */
    if (isset($_SESSION['authentication']) && $_SESSION['authentication'] > 0) {
        $cur_album = $_GET['album_id'];
        $new_quantity = intval($_GET['amount']);

        /* If the new quantity of a product is zero, then we have to remove the product
        * from the shopping cart. Otherwise we have to update the amount of a product
        * that is shown in the shopping cart.
        */
        if ($new_quantity < 1) {
            header("Location: rem_from_cart.php?del_album_id=$cur_album . ");
        }
        else {
            $update_q_query = $db->prepare('UPDATE winkelmand SET aantal=? WHERE album_album_id=?;');
            $update_q_query->bindValue(1, $new_quantity, PDO::PARAM_INT);
            $update_q_query->bindValue(2, $cur_album, PDO::PARAM_INT);
            $update_q_query->execute();

            header("Location: ../shopping_cart.php");
        }
    }
?>