<?php

session_start();

include 'opendb2.php';
include 'jem_queries.php';

/* Check if a user is logged in. */
if (isset($_SESSION['authentication']) && $_SESSION['authentication'] > 0) {
    /* If the shopping cart is filled, empty the shopping cart. */
    if (isset($_GET['del_album_id'])) {
        $cur_user = intval($_SESSION['user_id']);

        $cur_del_album = intval($_GET['del_album_id']);

        $rem_cart_query = $db->prepare(del_album_cart());

        $rem_cart_query->bindValue(1, $cur_user, PDO::PARAM_INT);
        $rem_cart_query->bindValue(2, $cur_del_album, PDO::PARAM_INT);
        $rem_cart_query->execute();

        header("Location: ../shopping_cart.php");
    } else {
        header("Location: ../index.php");
    }
}
