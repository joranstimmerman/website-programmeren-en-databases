<?php

/* Query to get the details of a specific album */
function product_query()
{
    $query = "SELECT * FROM album
              INNER JOIN genre ON album.genre_genre_id = genre.genre_id
              WHERE album_id = ?";
    return $query;
}

/* Query to get a products image */
function product_image_query()
{
    $query = "SELECT album.album_cover FROM bestelling_has_album
              INNER JOIN album ON album.album_id = album_album_id
              INNER JOIN bestelling
              ON bestelling.bestelling_id = bestelling_bestelling_id
              WHERE bestelling.gebruiker_gebruiker_id =?
              LIMIT 1";
    return $query;
}

/* Query to get the orders total price */
function get_order_total_query()
{
    $query = "SELECT SUM(aantal * album.prijs) AS order_total
              FROM bestelling_has_album
              INNER JOIN album ON album.album_id = album_album_id
              INNER JOIN bestelling
              ON bestelling.bestelling_id = bestelling_bestelling_id
              WHERE bestelling.bestelling_id = ?
              GROUP BY bestelling_bestelling_id";
    return $query;
}

/* Query to get the 4 most recent albums */
function most_recent_query()
{
    $query = "SELECT album_id, album_cover, titel, artiest, prijs
              FROM album
              ORDER BY datum_uitgave DESC
              LIMIT 4";
    return $query;
};

/* Query to get the most sold products */
function most_sold_query()
{
    $query = "SELECT album_cover, titel, artiest, prijs, album_id
              FROM bestelling_has_album
              INNER JOIN album ON album.album_id = album_album_id
              GROUP BY album_album_id
              ORDER BY SUM(aantal) DESC LIMIT 4";
    return $query;
};

/* Query to get all orders from a user */
function get_user_orders()
{
    $query = "SELECT * FROM bestelling
              INNER JOIN bestelling_status
              ON bestelling.bestelling_status_status_id = bestelling_status.status_id
              WHERE bestelling.gebruiker_gebruiker_id = ?
              ORDER BY bestel_datum DESC, bestelling_id DESC";
    return $query;
}

/* Query to get all albums that the user ordered */
function get_user_order_albums_query()
{
    $query = "SELECT * FROM bestelling_has_album
              INNER JOIN bestelling
              ON bestelling.bestelling_id = bestelling_bestelling_id
              INNER JOIN album
              ON album_album_id = album.album_id
              WHERE bestelling_bestelling_id = ?";
    return $query;
}

/* Query to get the first ordered album of a customer */
function get_first_user_order_albums_query()
{
    $query = "SELECT * FROM bestelling_has_album
              INNER JOIN bestelling
              ON bestelling.bestelling_id = bestelling_bestelling_id
              INNER JOIN album
              ON album_album_id = album.album_id
              WHERE bestelling_bestelling_id = ?
              LIMIT 1";

    return $query;
}

/* Query to get 4 albums related to a albums based on genre */
function get_related_albums()
{
    $query = "SELECT * FROM album
              WHERE genre_genre_id = ? AND album_id != ?
              LIMIT 4;";
    return $query;
}

/* Query to add a new album to the database. */
function add_album_query()
{
    $query = "INSERT INTO album (titel, artiest, prijs, datum_uitgave,
              omschrijving, album_cover, genre_genre_id)
              VALUES(?, ?, ?, ?, ?, ?, ?);";

    return $query;
}

/* Query to add a user and its details to the database. */
function add_customer_query()
{
    $query = "UPDATE gebruiker
              SET voornaam = ?, tussenvoegsel = ?, achternaam = ?,
              straatnaam = ?, huisnummer = ?, huisnummer_toevoeging = ?,
              postcode = ?, plaats = ?, email = ?
              WHERE gebruiker_id = ? ";
    return $query;
}

/* Query to get the paymethods from the database */
function get_paymethods()
{
    $query = "SELECT betaal_methode_id, betaal_methode_naam
              FROM betaal_methode;";
    return $query;
}

/* Query to get the corresponding payment method of a payment method ID */
function get_payname_by_id()
{
    $query = "SELECT betaal_methode_naam FROM betaal_methode
              WHERE betaal_methode_id =?";
    return $query;
}

/* Query to get the products in the users shopping cart */
function get_user_cart()
{
    $query = "SELECT album_album_id, datum_uitgave, titel, aantal, prijs,
              omschrijving, artiest, album_cover
              FROM winkelmand INNER JOIN album ON album_id = album_album_id
              WHERE gebruiker_gebruiker_id = ?";
    return $query;
}

/* Query to delete the users shopping cart */
function del_album_cart()
{
    $query = "DELETE FROM winkelmand WHERE gebruiker_gebruiker_id = ?
              AND album_album_id= ?";
    return $query;
}

/* Query to add a album to users shopping cart */
function add_album_cart()
{
    $query = "INSERT INTO winkelmand (gebruiker_gebruiker_id, album_album_id, aantal)
              VALUES (?, ?, ?)";
    return $query;
}

/* Query to get the users shopping cart */
function check_user_cart()
{
    $query = "SELECT * FROM winkelmand WHERE gebruiker_gebruiker_id = ?
              AND album_album_id= ?";
    return $query;
}

/* Query to get the total price of a users shopping cart */
function get_cart_total_query()
{
    $query = "SELECT SUM(aantal * album.prijs) AS order_total FROM winkelmand
              INNER JOIN album ON album.album_id = album_album_id
              WHERE gebruiker_gebruiker_id = ?";
    return $query;
}

/* Query to get a user his details */
function get_user_query()
{
    $query = "SELECT * FROM gebruiker WHERE gebruiker_id = ?";
    return $query;
}

/* Query to set the order it's state */
function set_order_status()
{
    $query = "UPDATE bestelling SET bestelling_status_status_id = ?
              WHERE bestelling_id = ?";
    return $query;
}

/* Query to get all genres */
function get_genre()
{
    $query = "SELECT * FROM genre;";
    return $query;
}

/* Query to get the corresponding text of a status ID */
function get_status_text()
{
    $query = "SELECT status_tekst FROM bestelling_status WHERE status_id = ?";
    return $query;
}
/* Query to get the albums determined by the given filters */
function get_albums()
{
    $query = "SELECT album_cover, titel, artiest, prijs, album_id FROM album";
    return $query;
}

/* Query to check if a user with a certain e-mailaddress already exists */
function check_duplicate()
{
    $query = "SELECT gebruiker_id, email, account_type FROM gebruiker
              WHERE email=?";
    return $query;
}

/* Query to add a user to the database */
function insert_user()
{
    $query = "INSERT INTO gebruiker (email, wachtwoord) VALUES ( ?, ? )";
    return $query;
}

/* Query to get a users ID and hashed password, to log in */
function user_login()
{
    $query = "SELECT gebruiker_id, wachtwoord FROM gebruiker WHERE email=?";
    return $query;
}

/* Query to get the account type of a user */
function user_account_type() {
    $query = "SELECT account_type FROM gebruiker WHERE  email = ?";
    return $query;
}

/* Query to get all status ids and corresponding status texts */
function get_status()
{
    $query = "SELECT * FROM bestelling_status";
    return $query;
}

/* Query to edit a product. */
function edit_album_query()
{
    $query = "UPDATE album
              SET titel = ?, artiest = ?, prijs = ?, datum_uitgave = ?,
              omschrijving = ?, album_cover = ?, genre_genre_id =?
              WHERE album_id = ?;";
    return $query;
}

/* Query to get the genre name of an album. */
function get_product_genre()
{
    $query = "SELECT genre_id, genre_naam FROM genre
              INNER JOIN album ON genre.genre_id = album.genre_genre_id
              WHERE album_id =?;";
    return $query;
}

/* Query to remove an album. */
function remove_query()
{
    $query = "UPDATE album SET is_leverbaar = 0 WHERE album_id = ?;";
    return $query;
}

/* Query to get all orders. */
function get_all_orders()
{
    $query = "SELECT * FROM bestelling
              ORDER BY bestel_datum DESC, bestelling_id DESC";
    return $query;
}

/* Query to get user info for admin accounts, when user ID is filled in. */
function get_user_account_id()
{
    $query = "SELECT * FROM gebruiker WHERE gebruiker_id = ?;";
    return $query;
}

/* Query to get user info for admin accounts, when last name is filled in. */
function get_user_account_name()
{
    $query = "SELECT * FROM gebruiker WHERE achternaam = ?;";
    return $query;
}

/* Query to get all user info. */
function get_all_users()
{
    $query = "SELECT * FROM gebruiker;";
    return $query;
}

/* Query to set the account type of a user. */
function set_account_type()
{
    $query = "UPDATE gebruiker SET account_type = ? WHERE gebruiker_id = ?;";
    return $query;
}

/* Query to get the reason for a returned product */
function get_retour_reden()
{
    $query = "SELECT * FROM retour_reden";
    return $query;
}

/* Query to get the amount of a product that was ordered */
function get_number_ordered_albums()
{
    $query = "SELECT aantal, album_album_id FROM bestelling_has_album
              WHERE bestelling_bestelling_id = ?";
    return $query;
}

/* Query to update album info when no image is given. */
function no_image_edit()
{
    $query = "UPDATE album
              SET titel = ?, artiest = ?, prijs = ?, datum_uitgave = ?,
              omschrijving = ?, genre_genre_id =?
              WHERE album_id = ?;";
    return $query;
}

/* Query to get all return status ids and corresponding status texts. */
function get_retour_status()
{
    $query = "SELECT * FROM retour_status";
    return $query;
}

/* Query to get the corresponding return status text of a status ID */
function get_retour_status_text()
{
    $query = "SELECT retour_status_tekst FROM retour_status
              WHERE retour_status_id = ?";
    return $query;
}

/* Query to set the status of a returned product */
function set_retour_status()
{
    $query = "UPDATE retour_verzending SET retour_status_status_id = ?
              WHERE retour_id = ?";
    return $query;
}

/* Query to get all albums from a user's shopping cart. */
function get_cart()
{
    $query = "SELECT album_album_id, aantal FROM winkelmand
              WHERE gebruiker_gebruiker_id=?";
    return $query;
}

/* Query to get the highest order ID */
function get_highest_order_id()
{
    $query = "SELECT bestelling_id from bestelling ORDER BY bestelling_id
              DESC LIMIT 1";
    return $query;
}

/* Query to insert an order into the database. */
function place_order()
{
    $query = "INSERT INTO bestelling(bestelling_id, bestel_datum,
              bestelling_status_status_id, betaal_methode_betaal_methode_id,
              gebruiker_gebruiker_id)
              VALUES (?, CURDATE(), ?, ?, ?)";
    return $query;
}

/* Query to insert the albums of an order into the database. */
function insert_order_albums()
{
    $query = "INSERT INTO bestelling_has_album(bestelling_bestelling_id,
              album_album_id, aantal)
              VALUES(?, ?, ?)";
    return $query;
}

/* Query to empty a user's shopping cart. */
function empty_cart()
{
    $query = "DELETE FROM winkelmand WHERE gebruiker_gebruiker_id=?";
    return $query;
}

/* Query to insert a return. */
function insert_return()
{
    $query = "INSERT INTO retour_verzending(datum, opmerking,
              bestelling_bestelling_id, retour_reden_reden_id)
              VALUES (CURDATE(), ?, ?, ?) ";
    return $query;
}

/* Query to get the highest retun ID */
function get_highest_return_id()
{
    $query = "SELECT retour_id FROM retour_verzending
              ORDER BY retour_id DESC LIMIT 1";
    return $query;
}

/* Query to add returned products to the database. */
function insert_returned_albums()
{
    $query = "INSERT INTO album_has_retour_verzending(album_album_id,
              retour_verzending_retour_id, aantal_retour)
              VALUES(?, ?, ?)";
    return $query;
}

/* Query to get a user's authentication code. */
function get_authentication()
{
    $query = "SELECT verificatie_code, account_type, email FROM gebruiker
              WHERE gebruiker_id=?";
    return $query;
}

/* Query to set new authentication code. */
function set_authentication()
{
    $query = "UPDATE gebruiker
              SET verificatie_code =?, verificatie_release =
              NOW() + INTERVAL 10 MINUTE
              WHERE gebruiker_id=?";
    return $query;
}

/* Query to updat the quantity of the product in the shopping cart. */
function update_cart_product()
{
    $query = "UPDATE winkelmand SET aantal=? WHERE gebruiker_gebruiker_id=?
              AND album_album_id=?";
    return $query;
}

/* Query to get verification details of a user. */
function get_verification_details()
{
    $query = "SELECT account_type, verificatie_code, verificatie_release
              FROM gebruiker WHERE gebruiker_id=?";
    return $query;
}

/* Query to set verification of a user. */
function set_verification()
{
    $query = "UPDATE gebruiker SET account_type = 1, verificatie_release=NOW()
              WHERE gebruiker_id=?";
    return $query;
}

/* Query to get some user details. */
function get_user_details()
{
    $query = "SELECT voornaam, achternaam, straatnaam, huisnummer, postcode
              FROM gebruiker WHERE gebruiker_id=?";
    return $query;
}

function search_query()
{
    $query = "SELECT titel, artiest, album_id, album_cover FROM album
              WHERE (titel LIKE ?) OR (artiest LIKE ?) LIMIT 5";
    return $query;
}

function get_first_retour_order()
{
    $query = "SELECT * FROM retour_verzending
              INNER JOIN album_has_retour_verzending
              ON retour_verzending.retour_id = album_has_retour_verzending.retour_verzending_retour_id
              INNER JOIN album
              ON album_has_retour_verzending.album_album_id = album.album_id
              WHERE retour_id = ? LIMIT 1";
    return $query;
}

function get_retour_status_for_retour()
{
    $query = "SELECT * FROM retour_reden WHERE reden_id = ? LIMIT 1";
    return $query;
}

function get_retour_albums()
{
    $query = "SELECT * FROM album_has_retour_verzending
              INNER JOIN album
              ON album_has_retour_verzending.album_album_id = album.album_id
              INNER JOIN retour_verzending
              ON album_has_retour_verzending.retour_verzending_retour_id = retour_verzending.retour_id
              WHERE retour_id = ?";
    return $query;
}

function get_status_retour()
{
    $query = "SELECT * FROM retour_status WHERE retour_status_id = ? LIMIT 1";
    return $query;
}

function get_all_users_retours()
{
    $query = "SELECT * FROM retour_verzending
              INNER JOIN bestelling
              ON retour_verzending.bestelling_bestelling_id = bestelling.bestelling_id
              INNER JOIN gebruiker
              ON bestelling.gebruiker_gebruiker_id = gebruiker.gebruiker_id
              WHERE gebruiker.gebruiker_id = ?
              ORDER BY datum DESC, retour_id DESC";
    return $query;
}

function get_retour_reden_text() {
    $query = "SELECT * FROM retour_reden WHERE reden_id = ?";
    return $query;
}