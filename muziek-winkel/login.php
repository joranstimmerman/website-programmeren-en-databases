<!doctype html>
<html lang="en">

<head>
    <title>JEM Records | Inloggen en registreren</title>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/standard_page.css">
    <link rel="stylesheet" type="text/css" href="css/navbar.css">
    <link rel="stylesheet" type="text/css" href="css/footer.css">
    <link rel="stylesheet" type="text/css" href="css/login.css">
    <link rel="icon" href="images/ui_icons/tabicoon.png" type="image/png">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/myScripts.js"></script>
</head>

<body>
    <?php include 'phpinclude/header.php'; ?>

    <div id="main_content">
        <!-- Verify authenticaton and give the corresponding content -->
        <?php if (isset($_SESSION['authentication']) && $_SESSION['authentication'] > 0) {
                header("Location: 401.php");
            } else { ?>
        <div id="login_register">
            <div id="login">
                <h2> Inloggen </h2>
                <form method="post" action="<?php echo htmlspecialchars("php/user_login.php"); ?>">
                    <?php
                        if (isset($_SESSION['inlog_error'])) {
                            echo $_SESSION['inlog_error'];
                        }
                    ?>
                    <input placeholder="E-mail..." type="text" name="email" /><br>
                    <input placeholder="Wachtwoord.." type="password" name="password" /><br>
                    <button type="submit" name="login">Inloggen</button>
                </form>
                <p>Klik <a href="forgot_password_email.php">hier</a> als u uw wachtwoord vergeten bent</p>
            </div>
            <div id="create">
                <h2> Registreren </h2>
                <form method="post" action="<?php echo htmlspecialchars("php/user_register.php"); ?>">
                    <?php
                        if (isset($_SESSION['register_error'])) {
                            echo $_SESSION['register_error'];
                        }

                        if (isset($_SESSION['register_success'])) {
                            echo $_SESSION['register_success'];
                        }
                    ?>
                    <input id="email" name="emailaddress" type="text" placeholder="E-mail..." /><br>
                    <input id="first_pass" name="password" type="password" placeholder="Wachtwoord..." /><br>
                    <input id="second_pass" name="sec_password" type="password" placeholder="Herhaal wachtwoord..." /><br>
                    <button id="submit_btn" type="submit" name="register">Registeer</button>
                </form>
            </div>
        </div>
        <?php } ?>
    </div>

    <?php include 'phpinclude/footer.php'; ?>
</body>

</html>
