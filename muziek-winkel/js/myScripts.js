$(document).ready(function() {
    updateOverview();
});

function getSearchResults(str)
{
    var url="php/search.php";
    str = str.replace(/\n/g, " "); // change newlines into spaces
    url = url + "?search_value=" + str;
    url = url + "&sid=" + Math.random();

    $('#queryResult').load(url);
}

function updateOverview(str) {
    var datastring = $("#filter_form").serialize();

    var url = "php/product_overview_load.php?";
    url = url + datastring;
    url = url + "&sid=" + Math.random();

    $('#overview_content').load(url);
}

