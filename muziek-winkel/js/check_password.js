function checkEqualPassword()
{
    var submit_btn = document.getElementById("submit_btn");
    var email = document.getElementById("email");
    var first_password = document.getElementById("first_pass");
    var second_password = document.getElementById("second_pass");

    var msg = document.getElementById("password_equal");

    if (email.value == "") {
        msg.innerHTML = "E-mail is een verplicht veld!"
        submit_btn.enabled = false;
    }
    else {
        if (email.value == /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/) {
            if (first_password == "") {
                msg.innerHTML = "Wachtwoord is een verplicht veld!"
                submit_btn.enabled = false;
            }
            else {
                if (first_password.value === /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/) {
                    if (second_password == "") {
                        msg.innerHTML = "Herhaal wachtwoord is een verplicht veld!"
                        submit_btn.enabled = false;
                    }
                    else {
                        if (first_password.value === second_password.value)
                        {
                            msg.innerHTML = "";
                            submit_btn.enabled = true;
                        }
                        else
                        {
                            msg.innerHTML = "Wachtwoord komt niet overeen!"
                            submit_btn.enabled = false;
                        }
                    }
                }
                else {
                    msg.innerHTML = "Wachtwoord moet minimaal bestaan uit 1 getal, 1 hoofdletter, 1 kleine letter en 6 of meer karakters."
                    submit_btn.enabled = false;
                }
            }
        }
        else {
            msg.innerHTML = "E-mailadres is ongeldig! E-mail moet een @ en een . bevatten"
            submit_btn.enabled = false;
        }
    }
}
