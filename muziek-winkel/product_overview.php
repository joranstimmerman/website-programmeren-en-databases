<!doctype html>
<html lang="en">

<head>
    <title>JEM Records | Product overzicht</title>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/standard_page.css">
    <link rel="stylesheet" type="text/css" href="css/navbar.css">
    <link rel="stylesheet" type="text/css" href="css/footer.css">
    <link rel="stylesheet" type="text/css" href="css/product_overview.css">
    <link rel="icon" href="images/ui_icons/tabicoon.png" type="image/png">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/myScripts.js"></script>
</head>

<body>
    <?php include 'phpinclude/header.php'; ?>

    <?php
        include "php/opendb.php";
        include "php/jem_queries.php";

        /* Get genres from database */
        $genres = $db->query(get_genre());

        /* Initialize searchvariabele */
        $search_value = "";

        /* Check if searchbar is used */
        if (isset($_GET['search'])) {
            $search_value = $_GET['search'];
        }
    ?>

    <div id="filter_nav">
        <form id="filter_form" onchange="updateOverview()" action="<?php echo htmlspecialchars("php/product_overview_load?search=$search_value") ?>" method="GET">
            <div class="filter">
                <p class="fat">Sorteren op</p>
                <input type="hidden" name="search_value" value="<?php echo $search_value ?>">
                <!-- Make possible to sort albums -->
                <select id="sort "name="sort">
                    <option <?php if (isset($_POST['sort']) && $_POST['sort'] == 'titel_AZ') { ?> selected="true" <?php }; ?> value="titel_AZ">Titel A-Z</option>
                    <option <?php if (isset($_POST['sort']) && $_POST['sort'] == 'titel_ZA') { ?> selected="true" <?php }; ?> value="titel_ZA">Titel Z-A</option>
                    <option <?php if (isset($_POST['sort']) && $_POST['sort'] == 'artiest_AZ') { ?> selected="true" <?php }; ?> value="artiest_AZ">artiest A-Z</option>
                    <option <?php if (isset($_POST['sort']) && $_POST['sort'] == 'artiest_ZA') { ?> selected="true" <?php }; ?> value="artiest_ZA">artiest Z-A</option>
                    <option <?php if (isset($_POST['sort']) && $_POST['sort'] == 'prijshooglaag') { ?> selected="true" <?php }; ?> value="prijshooglaag">Prijs: Hoog naar Laag</option>
                    <option <?php if (isset($_POST['sort']) && $_POST['sort'] == 'prijslaaghoog') { ?> selected="true" <?php }; ?> value="prijslaaghoog">Prijs: Laag naar Hoog</option>
                </select>
            </div>

            <div class="filter dropdown">
                <button class="dropbtn" type="button">Categorie<img class="dropimg" src="images/ui_icons/arrow_down.png" alt="drop_img"></button>
                <div class="dropdown-content">
                    <?php while ($row = $genres->fetch(PDO::FETCH_ASSOC)) { ?>
                        <p class="list">
                            <?php echo $row['genre_naam'] ?>
                            <input class="check" id="genre" name="genre" value="<?php echo $row['genre_id'] ?>" type="radio" <?php if (isset($_POST['genre']) && $_POST['genre'] == $row['genre_id']) { ?> checked='checked' <?php } ?>>
                        </p>
                    <?php } ?>
                </div>
            </div>

            <div class="filter">
                <p class="fat list">Prijs van</p>
                <input class="maxminvalue" name="min_value" id="min_value" type="number" min="0" value="<?php echo isset($_POST['min_value']) ? $_POST['min_value'] : ""?>" placeholder="min...">
                <p class="fat list">tot</p>
                <input class="maxminvalue" name="max_value" id="max_value" type="number" value="<?php echo isset($_POST['max_value']) ? $_POST['max_value'] : ""?>" placeholder="max...">
            </div>

            <div class="filter">
                <p class="fat list"><a href="product_overview.php" >Wis alle filters</a></p>
            </div>
        </form>
    </div>

    <div id="main_content">
        <div id="overview">
            <h2>Producten</h2>
            <div id="overview_content"></div>
        </div>
    </div>

    <?php include 'phpinclude/footer.php'; ?>
</body>

</html>
