<!doctype html>
<html lang="en">

<head>
    <title>JEM Records | Mijn bestellingen</title>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/standard_page.css">
    <link rel="stylesheet" type="text/css" href="css/navbar.css">
    <link rel="stylesheet" type="text/css" href="css/footer.css">
    <link rel="stylesheet" type="text/css" href="css/orders.css">
    <link rel="icon" href="images/ui_icons/tabicoon.png" type="image/png">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/myScripts.js"></script>
</head>

<body>
    <?php include 'phpinclude/header.php'; ?>

    <?php
        include "php/opendb.php";
        include "php/jem_queries.php";

        /* Verify authentication and get the retours and their information from the current user from database */
        if (isset($_SESSION['authentication']) && $_SESSION['authentication'] > 0) {
            $cur_user = intval($_SESSION['user_id']);

            $retour_query = $db->prepare(get_all_users_retours());
            $retour_query->bindValue(1, $cur_user, PDO::PARAM_INT);
            $retour_query->execute();
        }
        else if (!isset($_SESSION['authentication']))
        {
            header("Location: 401.php");
        }
        else if ($_SESSION['authentication'] < 1)
        {
            header("Location: register_status.php");
        }
    ?>

    <div id="main_content">
        <div class="order_list">
            <h2>Mijn Retourneringen</h2>
            <?php if ($retour_query->rowCount() < 1) { ?>
                <p>U heeft nog geen retouren</p>
            <?php } else {
                    while ($retour_row = $retour_query->fetch(PDO::FETCH_ASSOC)) { ?>
                    <div class="order">
                        <div class="order_img">
                            <h3><?php echo $retour_row['datum'] ?></h3>
                            <!-- Get the first album cover from an order -->
                            <img src=" <?php
                            $first_retour_albums_query = $db->prepare(get_first_retour_order());
                            $first_retour_albums_query->bindValue(1, intval($retour_row['retour_id']), PDO::PARAM_INT);
                            $first_retour_albums_query->execute();
                            $first_retour_albums_row = $first_retour_albums_query->fetch(PDO::FETCH_ASSOC);

                            echo $first_retour_albums_row['album_cover'];
                            ?>" alt="<?php $first_retour_albums_row['titel'] ?>">
                        </div>
                        <div class="order_info">
                            <h3>Retour informatie</h3>
                            <p>Retournummer: <?php echo $retour_row['retour_id'] ?><br></p>
                            <p>Bestellingnummer: <?php echo $retour_row['bestelling_bestelling_id'] ?><br></p>
                            <p>Reden:
                                <?php
                                    $retour_redenen = $db->prepare(get_retour_status_for_retour());
                                    $retour_redenen->bindValue(1, intval($retour_row['retour_reden_reden_id']), PDO::PARAM_INT);
                                    $retour_redenen->execute();
                                    $retour_reden = $retour_redenen->fetch(PDO::FETCH_ASSOC);
                                    echo $retour_reden['reden_text'];
                                 ?><br>
                            </p>
                            <p>Opmerking: <?php echo $retour_row['opmerking'] ?><br></p>
                            <p>Status:
                                <?php
                                    $retour_statussen = $db->prepare(get_status_retour());
                                    $retour_statussen->bindValue(1, intval($retour_row['retour_status_status_id']), PDO::PARAM_INT);
                                    $retour_statussen->execute();
                                    $retour_status = $retour_statussen->fetch(PDO::FETCH_ASSOC);
                                    echo $retour_status['retour_status_tekst'];
                                ?><br>
                            </p>
                        </div>
                        <div class="order_info">
                            <h3>Producten</h3>
                            <div class="order_albums">
                                <table>
                                    <?php
                                        /* Get every album that corresponds with this retour */
                                        $retour_albums_query = $db->prepare(get_retour_albums());
                                        $retour_albums_query->bindValue(1, intval($retour_row['retour_id']), PDO::PARAM_INT);
                                        $retour_albums_query->execute();

                                        while ($retour_albums_row = $retour_albums_query->fetch(PDO::FETCH_ASSOC)) {
                                    ?>
                                        <tr>
                                            <td>
                                                <a href="product.php?album_id=<?php echo $retour_albums_row['album_id'] ?>">
                                                    <?php echo $retour_albums_row['artiest'] ?> -
                                                    <?php echo $retour_albums_row['titel'] ?>
                                                </a>
                                                <span style="float:right">: <?php echo $retour_albums_row['aantal_retour'] ?></span>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                <?php }
                    } ?>
        </div>
    </div>

    <?php include 'phpinclude/footer.php'; ?>
</body>

</html>
