<!doctype html>
<html lang="en">

<head>
    <title>JEM Records | Wachtwoord</title>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/standard_page.css">
    <link rel="stylesheet" type="text/css" href="css/navbar.css">
    <link rel="stylesheet" type="text/css" href="css/footer.css">
    <link rel="stylesheet" type="text/css" href="css/forget_password.css">
    <link rel="icon" href="images/ui_icons/tabicoon.png" type="image/png">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/myScripts.js"></script>
</head>

<body>
    <?php include 'phpinclude/header.php'; ?>
    <div id="main_content">

        <?php
        include 'php/opendb.php';
        include "php/jem_queries.php";

        /* Filter out bad input */
        function clean_input($input) {
        $input = trim($input);
        $input = strip_tags($input);
        return $input;
    }

        /* Verify authenticaton and give the corresponding content, also make it possible to change password for a specific e-mailaddress */
        if (isset($_GET['auth_code']) && isset($_GET['user_mail'])) {
            $user_mail = $_GET['user_mail'];
            $auth_code = $_GET['auth_code'];

            $check_mail_query = $db->prepare("SELECT gebruiker_id FROM gebruiker where email=?");
            $check_mail_query->bindValue(1, $_GET['user_mail'], PDO::PARAM_STR);
            $check_mail_query->execute();

            if ($check_mail_query->rowCount() > 0)
            {
                $check_mail_row = $check_mail_query->fetch(PDO::FETCH_NUM);

                $user_id = $check_mail_row[0];

                $check_auth_query = $db->prepare("SELECT wachtwoord_auth, wachtwoord_release FROM gebruiker WHERE gebruiker_id=?");
                $check_auth_query->bindValue(1, $user_id, PDO::PARAM_STR);
                $check_auth_query->execute();

                $check_auth_row = $check_auth_query->fetch(PDO::FETCH_NUM);

                $auth_code = $_GET['auth_code'];

                if (strtotime(date($check_auth_row[1])) > strtotime(date('Y-m-d H:i:s'))) {
                    if ($check_auth_row[0] == $auth_code) { ?>
                        <h2>Wachtwoord resetten</h2>
                        <p>Vul hieronder uw nieuwe wachtwoord in:</p>
                        <?php
                        if (isset($_POST['submit_pass'])) {
                            /* Check if the form is submitted and give errormessages if required fieds are empty or if the input is not valid */
                            $error = $f_pass = $s_pass = "";

                            if (empty($_POST['f_pass'])) {
                                $error = "Wachtwoord is een verplicht veld.";
                                echo "<p class='error'>$error</p><br>";
                            }
                            else {
                                $f_pass = clean_input($_POST["f_pass"]);

                                if (!preg_match("/^\S*(?=\S{6,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$/", $f_pass)) {
                                    $error = "Wachtwoord mag alleen letters en getallen bevatten en minimaal beschikken over minimaal 6 karakters, 1 getal een hoofdletter en een kleine letter.";
                                    echo "<p class='error'>$error</p><br>";
                                }
                                else {
                                    if (empty($_POST["s_pass"])) {
                                        $error = "Herhaal wachtwoord is een verplicht veld.";
                                        echo "<p class='error'>$error</p><br>";
                                    }
                                    else {
                                        $s_pass = clean_input($_POST["s_pass"]);

                                        if ($f_pass !== $s_pass) {
                                            $error = "Wachtwoord komt niet overeen.";
                                            echo "<p class='error'>$error</p><br>";
                                        }
                                        else {
                                            $set_pass_query = $db->prepare("UPDATE gebruiker SET wachtwoord=?, wachtwoord_release=NOW() WHERE gebruiker_id=?");
                                            $set_pass_query->bindValue(1, password_hash($_POST['f_pass'], PASSWORD_DEFAULT), PDO::PARAM_STR);
                                            $set_pass_query->bindValue(2, $user_id, PDO::PARAM_INT);
                                            $set_pass_query->execute();

                                            echo "<p id='success'>Wachtwoord succesvol veranderd. U kan nu inloggen.</p><br>";
                                        }
                                    }
                                }
                            }
                        }
                        ?>
                        <form method="post">
                            <label for="f_pass">Wachtwoord: </label><br>
                            <input id="f_pass" name="f_pass" type="password"><br>
                            <label for="s_pass">Herhaal wachtwoord: </label><br>
                            <input id="s_pass" name="s_pass" type="password"><br>
                            <input id="save_btn" name="submit_pass" type="submit" value="Opslaan">
                        </form>
                        <?php
                    } else { ?>
                        <h2>Waarchuwing</h2>
                        <p>De link die u gebruikt heeft is niet (meer) geldig.</p>

                    <?php }
                }
            }
            else
            { ?>
                <h2>Waarchuwing</h2>
                <p>De link die u gebruikt is verlopen.</p>
            <?php }
        }
        else
        {
            header("Location: 404.php");
        }

        ?>
    </div>

    <?php include 'phpinclude/footer.php'; ?>
</body>

</html>